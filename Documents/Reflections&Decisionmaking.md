Reflections & Decision-making
===========================

Week 3 Client meeting
------
1. Audited our latest version of product and gave suggestions.
2. Our clients pointed one mistake of our permission-code form: in the selection of degree, we set "bachelor" and "master", but there some students else studying for "diploma" and they are not belong to either. 
3. Our clients also told us during the beginning of this semester, there were a lot of students who sent dulplicated emails when they did not recieve reply in one or two days. And some students still sent email to ask permission code after week3, when the course selection is overdue. 


##### reflection:
In the group meeting:
1. Disscuss the ways to correct the degree selection part and voted: 
    the first one is to add two more selections: "bachelor diploma" and "master diploma";
    the second one is to change the previous ones to "undergraduate" and "postgraduate", which contains all kinds of students.
2. Discuss the ways to prevent sudents sending dulplicate emails and voted:
    add announcement such us "replyment may take 4 or 5 weeddays" to homepage;
    add two click box which followed with the announcement. If these click boxes is unselected, users cannot enter to the next page.
    
##### decision-making:
1. change the previous ones to "undergraduate" and "postgraduate", which contains all kinds of students. (unanimous approval)
2. add two click box which followed with the announcement. If these click boxes is unselected, users cannot enter to the next page.(5 votes) 
3. complete these changes before next client meeting.

##### outcomes:
1. Final version of permission-code form. 

Week 3 Tutorial
------
1. To know each other: new shadow group, new tutors and new classmates.
2. Introduce our project in the current stage to tutor.

##### reflection:
1. Stadardize document management;
2. Stadardize ways of group communication;
3. More clear and short reflection;

##### decision-making:
1. Use Slack to communicate to raplace Wechat and Google Drive
2. Combine Reflection and Decision-making document to one document: Reflection & Decision-making


Week 4 Tutorial
---------
We got a really low mark after the first audit in this semester. In this tutorial, we mainly discuss how to improve our group's performance. We choose several valuable feedbacks from our shadow groups, tutor and clients as references, which includes:
1. Didn't present "communication" part in our presentation (from tutor)
2. The structure and contents of presentation is not clear  (from tutor)
3. Members are absent for no reason (from tutor and shadow group)
4. Some grammar mistakes in the document  (from tutor)
5. Issues in the gitlab are mainly made by one or two people (shadow group)
6. The UI of our product is too simple and lack of beauty 

###### reflection&decision-making:
The bad grade of this assessment gave the  alarm, we held a serious and stormy group meeting immediately afer the tutorial. In the meeting, we refelcted to the problems we have which are listed above and made the related decisions by our leader:
1. Use Slack to communicate and forbiden other communicational softwares to oncentrade our communictation record, so that we can uuload it in the following presentation.
2. No one could be absent for no reasons, otherwise the leader will report to tutor.
3. In the following presentation which is in week6 and week9, team members must have a rehearsal.
4. All the group menbers must upload their work to gitlab instead of collecting it firstly and uploading by one person.
5. Beauty our UI of product
6. All the documents must be checked by other group members before being uploaded

###### output:
Different issues made by different team members.
New UI of our product.
People in charge: Jiawei Liu (front-end developer)
Use markdown to record instead of .doc or .pdf so that our tutor and shadow team could see our document directly in Gitlab.
People in charge: Liuyang Qin (Document Manager)


Week 4 Client meeting
-----------
We mainly talked about the "course arrangement" part with our clients. And asked for some advise. 

###### reflection&decision-making:
The most difficult part of course arrangement is fomulating the rules. There are so many possibilities and limitations, such as couse release time, Pre-courses, ACS's accreditation and so on. And our clients are not very familiar with this part. Thus they reccomended us to connect with stuffs in CMS, who are in charge of course arrangement.

###### output:
Booking a meeting with stuffs in CECS for some advice: Lilun contacted this person on Friday and he will attend our group meeting in week 5.
Person in charge: Yilun Liu (communicator)



Week 5 Client meeting
----------
Introduce our project to the holder of "themes", which is a project contains all course information. This meeting is to build the brige between "themes" and our groups, so that we could avoid some duplicate work. 

###### reflection&decision-making:
1.Requirement: rules could be modified and maintained by our clients because rules are changable.  Thus we will set a interface to let stuffs in CECS could change program rules as they want.
2. Whether can our team access to Thenme's database or use their API, so that we don't need to start from zero. The member of Themes need to confirm with his team leader. 


###### output:
1. We keep theme's email address so that they could inform us when they comfirm if we can use their API or database. 
    Person in charge: Yilun Liu (communicator)
2. This meeting will save our working time significantly if we could cooperate with Themes. 


Week 5 Tutorial:
------------
1. Disscuss for two top issues our group need to improve.
2. Confirm some details on testing part: testers, how to test and testing document.
3. Discuss the date when we will hand over the product.
4. Disscuss how to deliver our system to clients. (what the host will we use, who will pay for it if necessary, when we will deliver, deos anyone even someone overseas can access to the website?)
5. tools we can use: gitlab, gekens, shelf
6. We need to get a host. 
7. How to make our reflections&decision-making easier to understand. 

###### reflection&decision-making:
1. Actually we have done the private testing by ourselves, but we did it orally. So we need to edit it to a formal way: a testing document. 
2. The matter of host delays our schedule, mainly because of the inefficent communication between the host holder and us by email. So we decide to communicate with him more derectly. Go to his office if necessary. 
3. Give more detalis in the reflections&decision-making file: who decide it, who is responsible to the action, when the action will happen.

###### output:
1. Set the public key by Wenjie on the spot.
2. Make a testing report by week 6. 
    Person in Charge: Wangyang Luo (Test Manager)
3. Reallocate our roles: each one has two roles: the main role and secondary role, which could be viewed on the main page of our Gitlab. 


Week6 Client Meeting
-----------

1. Add the requirements from the client about course arrangement function.
   Add a question in the form, Having u attend the lecture or tutorial before?

   Provide the CECS staff with a feedback link, so they can give this link to students to collect some feedback

   Add a degree called "Bachelor of information technology"

2. Ask the client to do the test of our new system and collect the suggestion
3. Discuss the further plan of our "course arrangment" function


###### reflection&decision-making:
1. The first thing we are going to do is always to satisfy clients' requirement. 
2. Another important issue we discuss in these meeting is that we found another group (AI Course Selection with client from Accenture) was doing the similar work with our "course arrangement" function. 

###### output: 
1. Modify the code to satisfy clients' requirements by the end of this week. 
    People in charge: Jiawei Liu (front-end developer)
2. Send email to Charles to ask if we need to continue the work which is overlapped with others'. 
    Time: Monday, Week 6
    Person in charge: Yilun Liu (communicator)


Week7 Client Meeting
-----------
1. Ask our clients to help us popularize our websites, so that our users could help us test the system and give some valuable feedback. 
2. To comfirm with clients whether every course is 6 units. 

###### reflection&decision-making:
1. The number of students who will recieve the emails. It should not be what exactly we need because most of students may just ignore the emails. 

###### output: 
1.  Edit a file which contains a user guide and our website adress to more than 300 students from different degrees and programs.
     People in charge: Yilun Liu 
     Time: Friday, Week7
2. Add the logic to the back-ends, which can judge whether a student complete a degree. 



Week 7 Tutorial:
------------
1. Review 3 top useful feedback from last audit page: 
    a. Test Management 
    b. UI Design
    c. Function
    
###### reflection&decision-making:
1. Our test report is not famal and conprehensive enough. We need to improve it, make it contain: Data correction, Host access and fail cases.  The standard format should be:
    ID      | Description | Exp result | Pass/fail | Link to requirement |
    ------ |--------------|-------------|------------|-----------------------|

###### output: 
1. Complete the test report with standard format
        People in charge: Liuyang Qin
        Time: Wednesday, Week 8
        


Week 8 Clinet Meeting
-----------
Disscuss the content and design of poster. 

###### reflection&decision-making:
1. We decided to simplify the design.
2. The content should be included: 
Context of problem:
-------
### Heavy workload:
* Staff: 500+ Emails related to permission code request
* Student: Long reponse time 

### Low efficiency:
* Staff: missing required information
* Student: sending duplicated Email

Scope of problem:
-------
* Automatically generate permission code request
* Reduce duplicated Email
* Organise and collect request information from student
* Challenge: real-time update of course information

Approach of solution:
-------
* Django/ MySQL/ JavaScript/CSS
* Git
* Trallo/ Email
* Agile management

Impact of project:
-------
### Achievement:
##### Reduce workload:
* Staff: 

Avoid duplicated Email

Reduce processing time (from 5 min to 2 min per request Email)

* Student:

Convinient and efficient method to request permission code

### Future direction:
Automatically response request by AI technology (Expand system)

###### output: 
1. Complete the first version of poster 
    time: Week9 Tuesday
    person in charge: Penny
    
    

Week 9 Client Meeting
-----------
Disscuss the problem we met: our clients sent 300 emails to different students for asking to test our product and publicize it on piazza. But we only recieved one feedback.

###### reflection&decision-making:
We decided to organize some of our friends or classmates to help us to test.

###### output: 
1. Complete the test.
    time: by week 10
    people in charge: all group members 
    
    


Week 9 Tutorial
-------------
Show our poster to tutors and shadow group for some feedback and made the reflection.
Disscuss how to present in the next audit.

###### reflection&decision-making:
Poster:
Three screenshots of our product are too many


Content should be included in presentation:
1. How to make judgers give us a fair mark
2. How to prove we made the project under our control
3. To present whether the project has finished or will be finished in the end of this semester
4. Everybody has tasks to do and his or her performence 

###### output: 
1. Modify the first version of poster according to the feedbacks.
    time: by week 10
    person in charge: Penny
    
2. Make full preparation for the presentaion
    time: by Wednesday on week 10
    people in charge: all group members






    
    
    


        









