# Bindi Project - Australian National University
 <h1>Hosting Plan</h1>

 Email: U5969555@anu.edu.au

 Employee Name:	&emsp;&emsp;&emsp; Wenjie Sun &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp; Titles:	&emsp;Mr.


 Employee Number:	&emsp;&emsp; U5969555 &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp; Supervisor:	&emsp;Penny


| Task                         | Due         | Expect Hrs. | Status     | Total Hrs. |
| ---------------------------- |:-----------:|:-----------:|:----------:| ----------:|
| Apply A VM                   | Aug. 23rd   | Unknown     | Done       |            |
| Learn System Information     | Aug. 23rd   |   0.5       | Done       | 0.5        |
| Install Terminal Command     | Aug. 24th   |    2        | Done       | 2.5        |
| Install Apache2              | Aug. 25th   |    1        | Done       |1           |
| Modify Apache2 Configuration | Aug. 25th   |    2        | Done       | 2          |
| Publish A Test Web           | Aug. 25th   |    1        | Done       | 10         |
| Publish Bindi Project        | Aug. 27th   |    1        | Done       | 1          |


 Employee signature:&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;     Date: &emsp;&emsp;&emsp;	 Aug. 27th       

 Supervisor signature: 	&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;     Date:                


Back-end Develop Plan
-------

 Member:	Peini Zhu (6125835@anu.edu.au) /   Yilun Liu (u6111948@anu.edu.au)


| Task                         | Due         | Expect Hrs. | Status     | Total Hrs. |     Member |
| ---------------------------- |:-----------:|:-----------:|:----------:| ----------:| ----------:|
| Fix: Change GET to POST      | Aug. 20     |    2        | Done       | 1.5        |Peini Zhu   |
| Solve branch conflicts       | Aug. 22     |    1        | Done       | 1.5        |Yilun Liu   |
| Add 2 tick confirm box       | Aug. 24     |    1        | Done       | 0.5        |Yilun Liu   |
| Add free text box            | Aug. 24     |    1        | Done       | 0.5        |Yilun Liu   |
| Congfig server database      | Aug. 26     |    2        | Done       | 2          |Peini Zhu   |
| Support server configuration | Aug. 27     |    2        | Done       | 6          |Yilun Liu   |
| Add search box for courses   | Aug. 31     |    2        | Ongoing    | 2.5        |Yilun Liu   |
| Remind window for application| Aug. 31     |    2        | Done       | 2          |Peini Zhu   |
| Change sentences in web pages| Aug. 31     |    1        | Done       | 0.5        |Peini Zhu   |
| Database design and configure| Sep. 16     |    5        | Need confrim| 5          |Peini Zhu   |
| Back end logic for part2     | Sep. 16     |    10       | done    | 10          |Yilun Liu   |
| Admin demo for clients       | Sep. 16     |    20       | Ongoing    | 20         |Peini Zhu   |

###### Notes:
* Details of admin demo for clients still need to confirm with clients, but the idea of this accepted. (more details look up issues)
* Database design may have some change after communication with CIMS.
* Reminder Windows need UI improvement later.
* Search box for courses need UI improvement later.


Back-end Develop Plan (Admin part including permission codes and course arrange)
-------

 Member:	Peini Zhu (6125835@anu.edu.au) 


| Task                         | Due         | Expect Hrs. | Status     | Total Hrs. |     Member |
| ---------------------------- |:-----------:|:-----------:|:----------:| ----------:| ----------:|
| Admin Login for clients      | Sep. 16     |    2        | Ongoing    | 1          |Peini Zhu   |
| Add course informations      | Sep. 16     |    2        | Done       | 2          |Peini Zhu   |
| Delete course informations   | Sep. 16     |    2        | Done       | 2          |Peini Zhu   |
| Update course informations   | Sep. 16     |    5        | Done       | 5          |Peini Zhu   |
| Fix csrf token machnism      | Sep. 16     |    5        | Done    | 5          |Peini Zhu   |
| Show courses list from database | Sep. 21     |    3        | Ongoing      | 1         |Peini Zhu   |
| Show programs from databases   | Sep. 21     |    3        | Ongoing       | 2      |Peini Zhu   |
| Create program level rules model | Sep. 28     |    5        | Ongoing       | 2          |Peini Zhu   |
| Admin for course rules | Sep. 28     |    10        | Done       | 10        |Peini Zhu   |
| Poster | Oct. 5     |    20        | Done       | 20        |Peini Zhu   |

###### Notes:
* Function for whole admin confirm with client Sep. 17
* Complete the permission code admin part first ASAP, then deliver to test.
* UI improvement ASAP, then distribute to server before Sep. 25


Back-end Develop Plan (Course show part)
-------

 Member:	Yilun Liu (u6111948@anu.edu.au)
 
 
| Task                         | Due         | Expect Hrs. | Status     | Total Hrs. |     Member |
| ---------------------------- |:-----------:|:-----------:|:----------:| ----------:| ----------:|
| validation of a course plan  | Sep. 21     |    5        | done    | 6          |Yilun Liu   |
| Design structure of input data  | Sep. 25     |    3        | done    | 3          |Yilun Liu   |
| Generate a list of courses which satisfies the requirement of program | Sep. 30     |    6        | done    | 5          |Yilun Liu   |
| Data for testing | Oct. 2     |    3        | done    | 2          |Yilun Liu   |
| Add pre-course to the list  | Oct. 4     |    3        | done    | 3          |Yilun Liu   |
| Build a feasible course plan  | Oct. 9     |    20        | done    | 18          |Yilun Liu   |
| Interaction with database and frontend  | Oct. 14     |    6        | ongiong    |           |Yilun Liu   |


Front-end Develop Plan
-------
 Email: U5893577@anu.edu.au

 Employee Name:	&emsp;&emsp;&emsp; Jiawei Liu &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp; Titles:	&emsp;Mr.


 Employee Number:	&emsp;&emsp; U5893577 &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp; Supervisor:	&emsp;Penny


| Task                               | Due         | Expect Hrs. | Status     | Total Hrs. | Member |
| ----------------------------       |:-----------:|:-----------:|:----------:| ----------:|:------:|
| Configure Django&setup project     | Aug. 15th   |    4        | Done       | 5          |Jiawei Liu|
| Reconstruct index.html             | Aug. 20th   |    3        | Done       |     3.5    |Jiawei Liu|
| Update app of permission_code      | Aug. 23th   |    3        | Done       | 2.5        |Jiawei Liu|
| Modify css of all buttons          | Aug. 24th   |    1        | Done       |1.5         |Jiawei Liu|
| Simple interface of insert_course  | Aug. 24th   |    2        | Done       | 1.5        |Jiawei Liu|
| Setup course_arrangement           | Aug. 28th   |    1        | Done       | 1          |Jiawei Liu|
| Write the alert window             | Sep. 10th   |    3        | Done       |2.5         |Jiawei Liu|
| Modify the search box              | Sep. 12th   |    3        | Done       | 6          |Jiawei Liu|
| Complete the admin inferface       | Sep. 14th   |    7        | Done    |      2     |Jiawei Liu|
| Modify UI for user test            | Sep. 19th   |    1.5      | Done       |      3     |Jiawei Liu|
| Change background of index         | Sep. 26th   |    0.5      | Done    |      2     |Jiawei Liu|
| Add-Course function changed        | Oct. 6th   |    2      | Done    |      4     |Jiawei Liu|
| Uid in title                       | Oct. 7th   |    2      | Done    |      1     |Jiawei Liu|
| Redirect to uni email              | Oct. 8th   |    3      | Done    |      2     |Jiawei Liu|





Test Plan
-------

 Member:	Wangyang Luo (5957547@anu.edu.au) /   


| Task                               | Due         | Expect Hrs. | Status     | Total Hrs. |
| -----------------------------------|:-----------:|:-----------:|:----------:| ----------:|
| ###Creat test paln                                                                               
|    Creat units test cases          | Aug. 24     |    1        | Done       | 3          |
|    Creat integration test cases    | Aug. 24     |    2        | Done       | 1          |
|    Creat usability test cases      | Aug. 24     |    2        | Done       | 3          |
| ###Unit test                                                                                     
|    run unit test                   | Aug. 25     |    4        | Done       | 4          |
|    unit test fixed and retesting   | Aug. 26     |    4        | Done       | 4          |
| ###integration test                                                                              
|    integration smoke test          | Aug. 24     |    3        | Done       | 3          |
|    run cycle one integration test  | Aug. 26     |    2        | Done       | 2          |
|    integration test fixed          | Aug. 26     |    4        | Done       | 4          |
| ###User usability test                                                                          
|    Do client usability test        | Aug. 31     |    2        | Done       | 2.5        |
|    Do student usability test       | Sep. 22     |    20       | Ongoing    | Ongoing    |
|    Collect test result             | Sep. 22     |    2        | Ongoing    | Ongoing    |
|    usability test fixed            | Sep. 24     |    4        | Ongoing    | Ongoing    |


Detailed Usability Test Plan
-------

Task Scenarios:

1.	Type in all the necessary information to build a email draft
I am a ANU student major in master of computing, this is my first semester and I need a permission code to enroll a course but I am not really sure how to write a email formally and which information is needed to apply for a permission code. Now I need to use this system to create a email draft.

2.	Apply the permission codes for four different courses
Use the “Add” and “Delete” button to add or delete a course.

3.	Copy the email draft to outlook and send a email to CECS staff
Now I have created a email draft and I need copy this draft to my outlook and send it to CECS staff

4.	Submit a feedback to Group Bindi
I am a user of “permission code” system and I want to give Group Bindi some suggestions to fix a bug.

5.	Do above tasks in different browser 
I can use Google Chrome to log in the system and complete the tasks smoothly, but my another computer only have Firefox.


6.	Use “Permission code” system in different counties
I have to go back to China to handle things, so can I use “Permission code” system in China?




| List                                                        |task scenarios | Expect unms | Status     | Total Hrs. |
| ------------------------------------------------------------|:-------------:|:-----------:|:----------:| ----------:|
| ###client usability test                                    | 1-6           |    2        | Done       | 2          |  
|
| ###student usability test(for different degree)                                                            
|    Bachelor of Engineering (Honors)                         | 1-6           |    2        | Ongoing    | 1          |
|Bachelor of Engineering (Honors) (Research and Development)" | 1-6           |    2        | Ongoing    | 2          |
|Bachelor of Advanced Computing (Honors)                      | 1-6           |    2        | Ongoing    | 2          |            
|Bachelor of Advanced Computing  (Research and Development)   | 1-6           |    4        | Ongoing    | 2          |
|Bachelor of Applied Data Analytics                           | 1-6           |    4        | Ongoing    | 2          |
|Bachelor of Software Engineering                             | 1-6           |    2        | Ongoing    | 1          |
|Bachelor of Information Technology                           | 1-6           |    2        | Ongoing    | 1          |
|Master of Computing                                          | 1-6           |    2        | Ongoing    | 1          |            
|Graduate Diploma of Applied Data Analytics                   | 1-6           |    4        | Ongoing    | 2          |
|Master of Applied Data Analytics                             | 1-6           |    2        | Ongoing    | 1          |
|Master of Engineering in Digital Systems                     | 1-6           |    2        | Ongoing    | 1          |
|Master of Engineering in Macaronis                           | 1-6           |    4        | Ongoing    | 2          |            
|Master of Engineering in Photonics                           | 1-6           |    4        | Ongoing    | 2          |
|Master of Engineering in Renewable Energy Systems            | 1-6           |    4        | Ongoing    | 2          |
|Master of Innovation and Professional Practice               | 1-6           |    4        | Ongoing    | 2          |            
|Graduate Diploma of Cyber Security,  Risk Management         | 1-6           |    4        | Ongoing    | 2          |
|Master of Cyber Security, Strategy and Risk Management       | 1-6           |    4        | Ongoing    | 2          |


