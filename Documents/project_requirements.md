Requirement list
-------

##### Requirement 1: 
* Feature: Free text box of Student Name need to be filled by student
* Requirement: Alphabet accept only

##### Requirement 2: 
* Feature: Free text box of Student ID need to be filled by student.
* Requirement: Regular Expression with ‘UXXXXXXX’ (begin with capital ‘U’, with 7 numbers)

##### Requirement 3: 
* Feature: Selective list of degree need to be selected by student
* Requirement: Show 2 degree ‘Undergraduate’ and ‘Postgraduate’, instead of ‘Master’ or ‘Bachelor’.

##### Requirement 4: 
* Feature: Selective list of programs need to be selected by student
* Requirement: According to the selected degree, show programs under specific degree:
	

    Undergraduate:
    
	> Diploma of Computing 
	
    > Bachelor of Engineering (Honours)
    
    > Bachelor of Engineering (Honours) (Research and Development)
    
    > Bachelor of Advanced Computing (Honours)
    
    > Bachelor of Advanced Computing (Honours) (Research and Development)
    
    > Bachelor of Applied Data Analytics
    
    > Bachelor of Software Engineering (Honours)
    
    > Bachelor of Information Technology"
	
	Postgraduate:
	
	> Graduate Diploma of Computing 
	
    > Master of Computing
    
    > Master of Computing (Advanced)
    
    > Graduate Diploma of Applied Data Analytics
    
    > Master of Applied Data Analytics
    
    > Master of Engineering in Digital Systems and Telecommunications
    
    > Master of Engineering in Mechatronics
    
    > Master of Engineering in Photonics
    
    > Master of Engineering in Renewable Energy Systems
    
    > Master of Innovation and Professional Proctice
    
    > Graduate Diploma of Cyber Security, Strategy and Risk Management

    > Master of Cyber Security, Strategy and Risk Management

##### Requirement 5: 
* Feature: Selective list of programs need to be selected by student
* Requirement: Add ‘Other’ program for students from other Colleges, it will show a free text for inputting their program name.

##### Requirement 6: 
* Feature: Add 2 questions need to be answered by student
* Requirement: Q1. Have you attempted this course before? If the answer is ‘YES’, then continue asking Q2. Have you failed this course on a previous attempt? Otherwise, if the answer is ‘NO’, it is no need to ask Q2.

##### Requirement 7: 
* Feature: To reduce duplication Email, combine the different courses permission code request Emails from one student
* Requirement: Student could add no more than 4 course permission code request, as in most common situation, student could take 4 courses in each semester, it they need to overload, they should request via ISIS, instead of request from our system.

##### Requirement 8: 
* Feature: Searchable text box of Course code which student requiring
* Requirement: Only showing the courses need to apply for permission code. And show the
course list according to student applying time. If student apply this at the beginning of one year,
it will show all course require a permission code in this year, if student apply this at the
middle of one year, for example at the beginning of semester 2, it will only show second
semester course requires a permission code in this year.

##### Requirement 9: 
* Feature: Free text box for student to write some other information
* Requirement: Add a free text box.

##### Requirement 10: 
* Feature: Add two-tick box to confirm student accept the terms 
* Requirement: Only after student tick these two box, it will jump to the page with generated draft Email. 

Two terms: 
> ‘I confirm that the information I have provided in my request is true and correct.’ 

> ‘I understand that my request may take up to 7 days to process and I will not send multiple emails.’

##### Requirement 11: 
* Feature: Generate a draft Email according to information collected from student
* Requirement: 

Remind Information at head:

> Please use this draft Email to submit your request for a permission code. You can edit this text as necessary. Please confirm that all the information in the draft is correct, then copy it and send it to studentadmin.cecs@anu.edu.au through your ANU Student Email Account.’

> Please ensure that the title of your Email is Permission code request - Uxxxxxxx’

Template of Email:

> ‘Hi CECS Student Services,

> My name is Student Name (Uni ID). I am currently completing the Degree.

> I am requesting permission to enroll in the following course(s): Course code 1, Course code 2, Course code 3 ...

> I confirm that I have attempted the course(s): Course code 1 (with failure) Course code 2(without failure) previously.

> And I have not attempted the course(s): Course code 3

> I understand that my request may take up to 7 days to process

> More information input in text field…

> Regards, 

> Student Name’
	More comment: If student attempt Course code 1 before and also failed, it will show as ‘Course code 1 (with failure)’. If student attempt Course code 1 before but not failed, it will show as ‘Course code 2(without failure)’. Otherwise it will not show the previous attempt information.

##### Requirement 12: 
* Feature: Add an ANU Email link on button ‘YES’, allows student to copy the draft and then automatically jump to their ANU Email box to send this request Email.
* Requirement: Do not automatically send the Email via web page. The permission code request Email should be sent from ANU Email box. 

##### Requirement 13: 
* Feature: Add a reminder message for student if the request time is not proper at the beginning.
* Requirement: The permission code request can only be accepted 2 weeks before o-week and 2 weeks after o-week. Usually permission code request Email will not be processed in other time. However, if click the ‘continue’ button, it allows student continue request in some other special situation.


