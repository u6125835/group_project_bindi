Back-end Develop Plan (week 1 - week 3)
-------
| Task                         | Due         | Expect Hrs. | Status     | Total Hrs. |     Member |
| ---------------------------- |:-----------:|:-----------:|:----------:| ----------:| ----------:|
| Dynamic selective list     | July. 30     |    8        | Done       | 10        |Peini Zhu   |
| Change email template language     | July. 30     |    2        | Done       | 2        |Peini Zhu   |
| Adjust code structure      | Aug. 3     |    3        | Done       | 5         |Peini Zhu   |
| Connect with database      | Aug. 3     |    10       | Done       | 10        |Peini Zhu   |
| Show information from DB   | Aug. 10     |    5        | Done       | 5        |Peini Zhu   |



Back-end Develop Plan (week 3 - week 6)
-------

 Member:	Peini Zhu (6125835@anu.edu.au) /   Yilun Liu (u6111948@anu.edu.au)


| Task                         | Due         | Expect Hrs. | Status     | Total Hrs. |     Member |
| ---------------------------- |:-----------:|:-----------:|:----------:| ----------:| ----------:|
| Fix: Change GET to POST      | Aug. 20     |    2        | Done       | 1.5        |Peini Zhu   |
| Solve branch conflicts       | Aug. 22     |    1        | Done       | 1.5        |Yilun Liu   |
| Add 2 tick confirm box       | Aug. 24     |    1        | Done       | 0.5        |Yilun Liu   |
| Add free text box            | Aug. 24     |    1        | Done       | 0.5        |Yilun Liu   |
| Congfig server database      | Aug. 26     |    2        | Done       | 2          |Peini Zhu   |
| Support server configuration | Aug. 27     |    2        | Done       | 6          |Yilun Liu   |
| Add search box for courses   | Aug. 31     |    2        | Ongoing    | 2.5        |Yilun Liu   |
| Remind window for application| Aug. 31     |    2        | Done       | 2          |Peini Zhu   |
| Change sentences in web pages| Aug. 31     |    1        | Done       | 0.5        |Peini Zhu   |
| Database design and configure| Sep. 16     |    5        | Need confrim| 5          |Peini Zhu   |
| Back end logic for part2     | Sep. 16     |    10       | done    | 10          |Yilun Liu   |
| Admin demo for clients       | Sep. 16     |    20       | Ongoing    | 20         |Peini Zhu   |

###### Notes:
* Details of admin demo for clients still need to confirm with clients, but the idea of this accepted. (more details look up issues)
* Database design may have some change after communication with CIMS.
* Reminder Windows need UI improvement later.
* Search box for courses need UI improvement later.



Back-end Develop Plan (Admin part including permission codes and course arrange) (week 7 - week 8)
-------

 Member:	Peini Zhu (6125835@anu.edu.au)


| Task                         | Due         | Expect Hrs. | Status     | Total Hrs. |     Member |
| ---------------------------- |:-----------:|:-----------:|:----------:| ----------:| ----------:|
| Admin Login for clients      | Sep. 16     |    2        | Done    | 1.5         |Peini Zhu   |
| Add course informations      | Sep. 16     |    2        | Done       | 2          |Peini Zhu   |
| Delete course informations   | Sep. 16     |    2        | Done       | 2          |Peini Zhu   |
| Update course informations   | Sep. 16     |    5        | Done       | 5          |Peini Zhu   |
| Fix csrf token machnism      | Sep. 16     |    5        | Done    | 5          |Peini Zhu   |
| Admin for course rules | Sep. 28     |    10        | Done       | 10        |Peini Zhu   |
| Show courses list from database | Oct. 19     |    3        | Ongoing      | 1         |Peini Zhu   |
| Show programs from databases   | Oct. 19     |    3        | Ongoing       | 2      |Peini Zhu   |
| Create program level rules model | Oct. 19     |    5        | Ongoing       | 2          |Peini Zhu   |


###### Notes:
* Function for whole admin confirm with client Sep. 17
* Complete the permission code admin part first ASAP, then deliver to test.
* UI improvement ASAP, then distribute to server before Sep. 25
* Last 3 taks based on the reliable data in Database

Other things plan (week 9 - week 10)
-------

 Member:	Peini Zhu (6125835@anu.edu.au)
 
| Task                         | Due         | Expect Hrs. | Status     | Total Hrs. |     Member |
| ---------------------------- |:-----------:|:-----------:|:----------:| ----------:| ----------:|
| Poster | Oct. 5     |    20        | Done       | 20        |Peini Zhu   |
| Requirement list | Oct. 9     |    5        | Done       | 5        |Peini Zhu   |
| Feature improvement doc | Oct. 9     |    5        | Done       | 5        |Peini Zhu   |
| Landing page   | Oct. 10     |    5       | Done       | 8        |Peini Zhu   |

Handover plan (week 11 - week 12)
-------

 Member:	Peini Zhu (6125835@anu.edu.au)
 
| Task                         | Due         | Expect Hrs. | Status     | Total Hrs. |     Member |
| ---------------------------- |:-----------:|:-----------:|:----------:| ----------:| ----------:|
| Enter all data into the database | Oct. 19     |    10        | Todo       | /        |Peini Zhu   |
| Front-end web page data synchronize from database | Oct. 19     |    3        | Todo       | /        |Peini Zhu   |
| Add docstring for code | Oct. 26     |    5        | Todo       | /       |Peini Zhu   |
| Write code maintain instruction | Oct. 26     |    5        | Todo       | /        |Peini Zhu   |
| Write admin use nstruction | Oct. 26     |    2        | Todo       | /        |Peini Zhu   |

###### Notes:
* Ask client for the officail course documentation 
* Front-end web page data synchronizion based on the reliable data in Database

Back-end Develop Plan (Course show part)
-------

 Member:	Yilun Liu (u6111948@anu.edu.au)
 

| Task                         | Due         | Expect Hrs. | Status     | Total Hrs. |     Member |
| ---------------------------- |:-----------:|:-----------:|:----------:| ----------:| ----------:|
| validation of a course plan  | Sep. 21     |    5        | done    | 6          |Yilun Liu   |
| Design structure of input data  | Sep. 25     |    3        | done    | 3          |Yilun Liu   |
| Generate a list of courses which satisfies the requirement of program | Sep. 30     |    6        | done    | 5          |Yilun Liu   |
| Data for testing | Oct. 2     |    3        | done    | 2          |Yilun Liu   |
| Add pre-course to the list  | Oct. 4     |    3        | done    | 3          |Yilun Liu   |
| Build a feasible course plan  | Oct. 9     |    20        | done    | 18          |Yilun Liu   |
| Interaction with database and frontend  | Oct. 14     |    6        | ongiong    |           |Yilun Liu   |
