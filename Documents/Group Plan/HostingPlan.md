<h1>Hosting Plan</h1>

Email: U5969555@anu.edu.au

Employee Name:	&emsp;&emsp;&emsp; Wenjie Sun &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp; Titles:	&emsp;Mr.


Employee Number:	&emsp;&emsp; U5969555 &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp; Supervisor:	&emsp;Penny


| Task                         | Due         | Expect Hrs. | Status     | Total Hrs. |
| ---------------------------- |:-----------:|:-----------:|:----------:| ----------:|
| Apply A VM                   | Aug. 23rd   | Unknown     | Done       |            |
| Learn System Information     | Aug. 23rd   |   0.5       | Done       | 0.5        |
| Install Terminal Command     | Aug. 24th   |    2        | Done       | 2.5        |
| Install Apache2              | Aug. 25th   |    1        | Done       |1           |
| Modify Apache2 Configuration | Aug. 25th   |    2        | Done       | 2          |
| Publish A Test Web           | Aug. 25th   |    1        | Done       | 10         |
| Publish Bindi Project        | Aug. 27th   |    1        | Done       | 1          |


Employee signature:&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;     Date: &emsp;&emsp;&emsp;	 Aug. 27th

Supervisor signature: 	&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;     Date:
