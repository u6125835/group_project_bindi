Front-end Develop Plan
-------
 Email: U5893577@anu.edu.au

 Employee Name:	&emsp;&emsp;&emsp; Jiawei Liu &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp; Titles:	&emsp;Mr.


 Employee Number:	&emsp;&emsp; U5893577 &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp; Supervisor:	&emsp;Penny


| Task                               | Due         | Expect Hrs. | Status     | Total Hrs. | Member |
| ----------------------------       |:-----------:|:-----------:|:----------:| ----------:|:------:|
| Configure Django&setup project     | Aug. 15th   |    4        | Done       | 5          |Jiawei Liu|
| Reconstruct index.html             | Aug. 20th   |    3        | Done       |     3.5    |Jiawei Liu|
| Update app of permission_code      | Aug. 23th   |    3        | Done       | 2.5        |Jiawei Liu|
| Modify css of all buttons          | Aug. 24th   |    1        | Done       |1.5         |Jiawei Liu|
| Simple interface of insert_course  | Aug. 24th   |    2        | Done       | 1.5        |Jiawei Liu|
| Setup course_arrangement           | Aug. 28th   |    1        | Done       | 1          |Jiawei Liu|
| Write the alert window             | Sep. 10th   |    3        | Done       |2.5         |Jiawei Liu|
| Modify the search box              | Sep. 12th   |    3        | Done       | 6          |Jiawei Liu|
| Complete the admin inferface       | Sep. 14th   |    7        | Done    |      2     |Jiawei Liu|
| Modify UI for user test            | Sep. 19th   |    1.5      | Done       |      3     |Jiawei Liu|
| Change background of index         | Sep. 26th   |    0.5      | Done    |      2     |Jiawei Liu|
| Add-Course function changed        | Oct. 6th   |    2      | Done    |      4     |Jiawei Liu|
| Uid in title                       | Oct. 7th   |    2      | Done    |      1     |Jiawei Liu|
| Redirect to uni email              | Oct. 8th   |    3      | Done    |      2     |Jiawei Liu|
