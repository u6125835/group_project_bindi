**Server Maintenance Rules**
* Regular restart of the server.  
Each server is guaranteed to be restarted once a week. Review after restarting to confirm that the server has been started and all services on the server are back to normal. Take appropriate measures in case of failure to start up or service is not restored in time. Bindi server requires a remote login to the server to restart the service.

* Security and performance checks on the server.  
Each server is guaranteed to be checked at least twice a week. The results of each inspection require to be recorded. For temporary need to find security check software from the Interne, don't go to any unknown site to download, try to choose official web site to download. Save the tool to /home/users/u6125835/tools. After download, make sure the current antivirus software has been upgraded to the latest version and check the new software with the antivirus software. 

* Server data backup work.  
Ensure backup server system data once per month. For server application data, ensure backup at least once every two weeks and ensure monthly backup for server user data. The backups need to be saved at /home/users/u6125835/old folder. For different data, corresponding subfolders need to be built. For example, apache data need to put in ‘apache’ folder and user u5969555 data need to be put in ‘u5969555’ folder.

* Monitoring of the server  
The monitoring work of the server must be guaranteed during the normal operation of sever and corresponding measures should be taken once the service is found to be stopped. If find a service stop, firstly check whether the same type of service on the server is interrupted. If all the same type of service has been interrupted, login the server in time to see the related reasons and try to re-open the corresponding service for that reason.

* The related log operation of the server.  
Each server guarantees to clean the related log once a month, such as application log, security log and system log. Every log should be selected to save before cleaning. All log files are uniformly stored under /home/users/u6125835/log. All backup log files are named after the backup date, such as 20050824.log. For logs that are not single-file, create a date-named folder at the corresponding record location and store the files in that folder.

* The server patch and application update work.   
For the newly issued vulnerability patch, the security update of the application must be applied to each server as soon as it is discovered.

* Any time related work.  
Every server must notify all administrators of the need to install new applications or uninstall applications due to application changes or other reasons.

* Make record of management.  
For each server management, set up a server administrator log in every time. The detailed records need to record the following items: login time, out of time, when the login server status (contains unknown process records, port connection state, the system account status, memory/CPU state) and detailed operation situation record. Both the remote login and physical contact operations need to be recorded, and then these records should be filed on each server in chronological order.
