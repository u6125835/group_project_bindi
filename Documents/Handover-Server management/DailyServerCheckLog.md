&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;**Daily Server Check Log**


&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;Contact Email: U5969555@anu.edu.au

|         | Monday      | Tuesday     | Wednesday    | Thursday | Friday | Saturday | Sunday |
| :-------: |:-----------:|:-----------:|:------------:| :--------:| :------:| :--------:| :------:|
| Week 5  |    |             |           |           | CSS error|||
| Week 6  | ✔  |    ✔        | 500 error | ✔  | ✔ | ✔| ✔|
| Week 7  | ✔  |    ✔        | ✔         | 400 error | 400 error | 400 error |✔|
| Mid Break  | ✔  |    ✔        | ✔         | ✔  | ✔ | ✔ | ✔ |
| Mid Break  | ✔  |    ✔        | ✔         | ✔ | ✔ | ✔ | ✔ |
| Week 8  | ✔  |    ✔        | ✔         | ✔ | ✔ | ✔ | ✔ |
| Week 9  | ✔  |    ✔        | ✔         | ✔ | ✔ | ✔ | ✔ |
| Week 10 | ✔  |    ✔        |           |           ||||
| Week 11 |    |             |           |           ||||
| Week 12 |    |             |           |           ||||
<br><br>