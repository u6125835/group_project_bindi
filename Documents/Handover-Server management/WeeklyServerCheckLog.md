&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;**Weekly Server Condition Check Log**

&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;Contact Email: U5969555@anu.edu.au

|    Date     | Operate System  | Storage     | Power    | Network |
| :-------: |:-----------:|:-----------:|:------------:| :--------:|
| Week 5  |  ✔  |   ✔          |   ✔        |   ✔        |
| Week 6  | ✔  |    ✔        | ✔ | ✔  |
| Week 7  | ✔  |    ✔        | ✔         | ✔|
| Mid Break  | ✔  |    ✔        | ✔         | ✔  |
| Mid Break  | ✔  |    ✔        | ✔         | ✔ |
| Week 8  | ✔  |    ✔        | ✔         | ✔ |
| Week 9  | ✔  |    ✔        | ✔         | ✔ |
| Week 10 |   |            |           |           |
| Week 11 |    |             |           |           |
| Week 12 |    |             |           |           |