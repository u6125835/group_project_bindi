Version 2.0
------

Open the homepage, enter first name, last name and uni id:

![Images](https://gitlab.cecs.anu.edu.au/u6125835/group_project_bindi/raw/master/Documents/Images/1.png "1")

Choose the degree and program:

![Images](https://gitlab.cecs.anu.edu.au/u6125835/group_project_bindi/raw/master/Documents/Images/2.png "2")

If you choose the Postgraduate, it will only show programs under Postgraduate, as follow (same in Undergraduate):

![Images](https://gitlab.cecs.anu.edu.au/u6125835/group_project_bindi/raw/master/Documents/Images/3.png "3")

If students are from other Colleges, it allow free text for inputing their program name:

![Images](https://gitlab.cecs.anu.edu.au/u6125835/group_project_bindi/raw/master/Documents/Images/4.png "4")

Enter the informations of the course which is applied for the permission code:

![Images](https://gitlab.cecs.anu.edu.au/u6125835/group_project_bindi/raw/master/Documents/Images/5.png "5")

If stundents need to apply more than one course , click the “Add/Delete Course” to add or remove courses:

![Images](https://gitlab.cecs.anu.edu.au/u6125835/group_project_bindi/raw/master/Documents/Images/6.png "6")

The maximum number of courses that could be applied within one application is 4. After all courses are added, click the “Confirm” button to generate an email draft, make sure all the information is correct, then click button 'copy', it will comes a alert window about "copy completed':

![Images](https://gitlab.cecs.anu.edu.au/u6125835/group_project_bindi/raw/master/Documents/Images/7.png "7")

Open your own ANU account email box page and create a new email sending to studentadmin@anu.edu.au with a title as “Request Permission Code(s) uxxxxxxx” (with your uni id), paste the draft email in the content blank:

![Images](https://gitlab.cecs.anu.edu.au/u6125835/group_project_bindi/raw/master/Documents/Images/0.png "0")

Then send the email.

Click the feedback button:

![Images](https://gitlab.cecs.anu.edu.au/u6125835/group_project_bindi/raw/master/Documents/Images/8.png "8")

Please give us a feedback cooment about how you feeling this draft, which would help us to improve it later:

![Images](https://gitlab.cecs.anu.edu.au/u6125835/group_project_bindi/raw/master/Documents/Images/9.png "9")


##### Note:
Please DO NOT send duplicated Email for same application.
Please use your ANU Student Email Account to send that permission code application email.
Please request the permission code before week1 Friday. 


