#  Test Report
##  Bindi -- Permission code function

| ID  | Description                                                   | Exp result                                                | Pass/fail | Link to requirement   |
| --- | :------------------------------------------------:| :--------------------------------------------: | :--------: | -----------------------: |
|1    |The student ID doesn't start with "U"           | Rejected. Display: Error Massage           | Fail       | Requirement No.4     |
|2    |The digit of Uni ID is not 7                             | Rejected. Display: Error Massage           | Fail      | Requirement No.4     |
|3    |The digit of Uni ID is 7 but not correct          | Accept                                                     | Pass       | Requirement No.4    |
|4    |There are numbers in First Name                  | Rejected. Cannot be inputted          | Pass       | Requirement No.4    |
|5    |There are numbers in Last Name                  | Rejected. Cannot be inputted           | Pass       | Requirement No.4    |
|6    |There are more than English text in First Name | Rejected. Cannot be inputted       | Pass       | Requirement No.4    |
|7    |There are more than English text in Last Name | Rejected. Cannot be inputted       | Pass       | Requirement No.4    |
|8    |Wrong information in the course name         | Accept                                                     | Pass       | Requirement No.7    |
|9    |Right course information but not includedin the semester or degree    | Accept            | Pass       | Requirement No.7    |
|10  |Login in different places not only in ACT      | Accept                                                    | Fail           | Requirement No.1    |
|11  |Didn't select or fill in all the information in the form| Regected. Display: Please fill in the information| Pass | Requirement No.4|
|12  |Reedit the draft when double checking the  information| Accept| Pass | Requirement No.4|

Related feature requirements see <a href="https://gitlab.cecs.anu.edu.au/u6125835/group_project_bindi/blob/master/Documents/project_requirements.md">**here**</a>.
