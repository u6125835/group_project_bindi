Context of problem:
-------
### Heavy workload:
* Staff: 500+ Emails related to permission code request
* Student: Long reponse time 

### Low efficiency:
* Staff: missing required information
* Student: sending duplicated Email

Scope of problem:
-------
* Automatically generate permission code request
* Reduce duplicated Email
* Organise and collect request information from student
* Challenge: real-time update of course information

Approach of solution:
-------
* Django/ MySQL/ JavaScript/CSS
* Git
* Trallo/ Email
* Agile management

Impact of project:
-------
### Achievement:
##### Reduce workload:
* Staff: 

Avoid duplicated Email

Reduce processing time (from 5 min to 2 min per request Email)

* Student:

Convinient and efficient method to request permission code

### Future direction:
Automatically response request by AI technology (Expand system)