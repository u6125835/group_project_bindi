# Bindi Group Meeting Summary week 4 
   
 Meeting time: 10am-12am, Aug 15, 2018
 
 Location: Chifley Library, level 1
 
 Meeting Theme: interactive design
 
 Editor: Wangyang Luo
________________________________________
## Attendance:
+ Penny Zhu
+ Jiawei Liu 
+ Wangyang Luo 
+ Yilun Liu
+ LiuYang Qin
+ Wenjie Sun

________________________________________

## Meeting Overview
### 1.	Send the confirmation email to teamnates for the first audit:

        Location: ian ross R104
        
    	Time: 10.00am-12.00am
    	
### 2.	According to the clients’ requirement, we plan to finish the interactive design of Couse Arrangement function and we assign everyone's task.

    	Wangyang Luo will finish the meeting record and ask for the VM
    	
    	Liuyang Qin will responsible for the reflection document
    	
    	Penny Zhu,Jiawei Liu and Wenjie Sun will do the coding part
    	
### 3.	Design the UI demo for the course arrangment function