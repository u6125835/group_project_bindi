# Bindi Group Meeting Summary week3 
   
 Meeting time: 4.30pm-5pm, 6pm-9.30pm Monday, Aug 6, 2018
 
 Location: Chifley Library, level 1
 
 Meeting Theme: Prepare the first audit
 
 Editor: Wangyang Luo
________________________________________
## Attendance:
+ Penny Zhu
+ Jiawei Liu 
+ Wangyang Luo 
+ Yilun Liu
+ LiuYang Qin

________________________________________

## Meeting Overview
### 1.	Send the confirmation email to clients for the first audit:

        Location: ian ross R104
        
    	Time: 8.00am-10.00am
    	
### 2.	According to the clients’ requirement, we plan to finish the work flow diagram and user story mapping.

    	Start with the work flow
    	
    	Combination with different big activities
    	
    	Fill in the 24 user stories
    	
### 3.	Design the UML demo for the course arrangment function

