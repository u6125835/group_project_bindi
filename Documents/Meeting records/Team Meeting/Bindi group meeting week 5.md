# Bindi Group Meeting Summary week 5 
   
 Meeting time: 10am-12am, Aug 22, 2018
 
 Location: CSIT, n110
 
 Meeting Theme: Role assignment
 
 Editor: Wangyang Luo
________________________________________
## Attendance:
+ Penny Zhu
+ Jiawei Liu 
+ Wangyang Luo 
+ Yilun Liu
+ LiuYang Qin
+ Wenjie Sun

________________________________________

## Meeting Overview
### 1.	Send the confirmation email to teamnates for the first audit:

        Location: CSIT, n110
        
    	Time: 10.00am-12.00am
    	
### 2.	According to the clients’ requirement, we add two new team role "Test manager" and "Host deployment"

    	Responsibility of "Test manager":Writing testing reports and designing test units
    	
    	Responsibility of "Host deployment": Deployment plan and sechedule, maintain the host
    	
    	
### 3.	Solve the VM problem, we can not access to the server provided by Bob (keep communication with Bob)

### 4.	Reporting work last week and assign new taks for next week