# Bindi Group Client Meeting Summary week 5 
   
 Meeting time: 2.30pm-3.30pm, Aug 21, 2018
 
 Location: CSIT, N110
 
 Meeting Theme: ask the possibility to access to the college database and add more requirements 
 
 Editor: Wangyang Luo
________________________________________
## Attendance:
+ Penny Zhu
+ Jiawei Liu 
+ Wangyang Luo 
+ Yilun Liu
+ Wenjie Sun

________________________________________

## Meeting Overview
### 1.	Send the confirmation email to teamnates for the meeting:

        Location: CSIT, N110
        
    	Time: 2.30pm-3.30pm, Aug 27, 2018
    	
### 2.	Add the requirements from the client about course arrangement function.

    	Add a question in the form, Having u attend the lecture or tutorial before?
    	
    	Provide the CECS staff with a feedback link, so they can give this link to students to collect some feedback
    	
    	Add a degree called "Bachelor of information technology"
    	
    	
### 3.	Ask the client to do the test of our new system and collect the suggestion

### 4.	Discuss the further plan of our "course arrangment" function
