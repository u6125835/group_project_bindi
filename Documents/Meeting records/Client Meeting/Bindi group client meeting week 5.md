# Bindi Group Client Meeting Summary week 5 
   
 Meeting time: 2.30pm-3.30pm, Aug 21, 2018
 
 Location: CSIT, N110
 
 Meeting Theme: ask the possibility to access to the college database and add more requirements 
 
 Editor: Wangyang Luo
________________________________________
## Attendance:
+ Penny Zhu
+ Jiawei Liu 
+ Wangyang Luo 
+ Yilun Liu
+ LiuYang Qin
+ Wenjie Sun

________________________________________

## Meeting Overview
### 1.	Send the confirmation email to teamnates for the meeting:

        Location: CSIT, N110
        
    	Time: 2.30pm-3.30pm, Aug 21, 2018
    	
### 2.	Add the requirements from the client about course arrangement function.

    	As a Student Service Staff, Can i maintain the course and program rules when the rule changes
    	
    	As a Student Service Staff, It would be better if there is a log in page so only staffs can log in and maintain the rules.
    	
    	
### 3.	Ask the possibility if we can access the database of CECS to read the course information

### 3.	Modify the UI design of our system to make it looks more beautiful