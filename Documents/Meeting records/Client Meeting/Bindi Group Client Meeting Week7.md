# Bindi Group Client Meeting Summary week 7 
   
 Meeting time: 2.30pm-3.30pm, Sep 18, 2018
 
 Location: CSIT, N110
 
 Meeting Theme: ask the possibility to access to the college database and add more requirements 
 
 Editor: Wangyang Luo
________________________________________
## Attendance:
+ Penny Zhu
+ Jiawei Liu 
+ Wangyang Luo 
+ Yilun Liu
+ Wenjie Sun

________________________________________
-----------
1. Ask our clients to help us popularize our websites, so that our users could help us test the system and give some valuable feedback. 

2. To comfirm with clients whether every course is 6 units. 

3. The number of students who will recieve the emails. It should not be what exactly we need because most of students may just ignore the emails. 

4.  Edit a file which contains a user guide and our website adress to more than 300 students from different degrees and programs.
     People in charge: Yilun Liu 
     Time: Friday, Week7
5. Add the logic to the back-ends, which can judge whether a student complete a degree. 