# Bindi Group Client Meeting Summary week 4 
   
 Meeting time: 2.30pm-3.30pm, Aug 17, 2018
 
 Location: CSIT, N110
 
 Meeting Theme: ask feedback for interactive design from client
 
 Editor: Wangyang Luo
________________________________________
## Attendance:
+ Penny Zhu
+ Jiawei Liu 
+ Wangyang Luo 
+ Yilun Liu
+ LiuYang Qin
+ Wenjie Sun

________________________________________

## Meeting Overview
### 1.	Send the confirmation email to teamnates for the meeting:

        Location: CSIT, N110
        
    	Time: 2.30pm-3.30pm, Aug 17, 2018
    	
### 2.	Ask for the requirements from the client about course arrangement function.

    	As a Student Service Staff, it usually took 20 minutes to ask the transcript and what course have done, and which program students in? 
    	And half an hour to write down the study pattern for students.
    	
    	As a Student Service Staff, I usually received hundreds (estimate 250+) of Email from students especially new students 
    	for asking how to make their study pattern 2 weeks before O-week and 2 weeks after O-week.
    	
    	
### 3.	Do the demonstration for the client and ask some feedbacks