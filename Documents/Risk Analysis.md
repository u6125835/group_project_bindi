# Risk Assessment 
-----------------------------
We assessed risks by analyzing all the objective and subjective factors. All the risks have considered from the aspects of client, team, end user and ANU/ TechLauncher. We assessed the priority of this risks by team discussion so that we can allocate time more scientifically. We also used PSM (PRACTICAL SOFTWARE AND SYSTEMS MEASUREMENT) model to find effective measures to alleviate these risks.

## Risk Quantification   (1= low,  5= extreme)

| Risk Type                                  | Priority  |  Overall Rating |
| ----------------------------------- | --------- | ---------------- | 
| Financial loss                            | 1            |  Low               |
| Bugs                                         | 4            | Extreme          |
| Loss of program                       | 3            | High                |
| Change of requirements           | 4            | Extreme          |
| Damage to the ANU reputation or the reputation of clients| 4| Extreme |
| Failed project                           | 3            | Medium           | 
| Team changes                         | 2             | Medium           |
|  Unable to install                     | 2             | Low                 |
| Unable to use                         | 3             | Medium            |
| Project Delays                        | 5             | Extreme            |
| Reduced usability                   | 4            | Extreme            | 

## Measures Identification
PSM is an integrated technical discipline for implementing software, systems, and organizational measurement. Here are some important concepts of it:

Information need: What the measurement user (e.g., manager or project team member) needs to know
in order to make informed decisions.

Information Category: A logical grouping of information needs that are defined in PSM to provide structure for the Information Model. PSM categories include schedule and progress, resources and cost, product size and stability, product quality, process performance, technology effectiveness, and customer satisfaction. Categories are defined in Chapter 2 of the PSM book.

Measureable Concept: An idea for satisfying the information need by defining the entities and their attributes to be measured.

Information needs of our project:
· handover problem
· uncertainty of schedule
· lack of related    skills and experience
. limited working time

| Information Need:          | Information Category: | Measureable Concept:         | Perspective Measures:  |
| --------------------------- | ------------------------- | -------------------------------- | --------------------------- |
| handover problem         | Resources and cost    | Facilities and Support Resources | Reduce the usage rate of last group’s code |
| uncertainty of schedule | Schedule and Progress | Milestone Completion      | - Completion should be based on achieving specific quantifiable milestone completion criteria - Include updates as schedules change - Milestones may include inch stones, or major critical milestones|
| lack of related skills and experience | Product Quality | Functional Correctness | Contractor Performance Assessment or other survey |
| limited working time | Schedule and Progress | Work Backlog | Set “Bindi Day” on every Saturday  |


## Reference:

http://www.psmsc.com/Downloads/Other/ICM%20Table%20-%20v7.0a%20final%20-%20Oct%202012.pdf




















