Functional improvement of permission code part:
-------
### Improvement 1:
From the information provided by client, the permission code request can only be accepted 2 weeks before o-week and 2 weeks after o-week. Usually permission code request Email will not be processed in other time. After group meeting discussion, we find it is necessary to add a reminder message for student if the request time is not proper. However, the ‘continue’ button allows student continue request in some other special situation. The output of this reflection as below: 
![Images](https://gitlab.cecs.anu.edu.au/u6125835/group_project_bindi/raw/master/Documents/Images/functional_improvement/01.png "1")
### Improvement 2:
According to the requirement from client, it will be convenient for both student and client, if the program and degree could be selective. The information collected from student will be much former. So we decide to develop an dynamic selective list to show different programs when chose different degree. The output of this feature as below:
![Images](https://gitlab.cecs.anu.edu.au/u6125835/group_project_bindi/raw/master/Documents/Images/functional_improvement/02.png "2")
![Images](https://gitlab.cecs.anu.edu.au/u6125835/group_project_bindi/raw/master/Documents/Images/functional_improvement/03.png "3")
### Improvement 3:
As one of feedback from shadow group is that, it might be convenient for student if it only shows the course could be applied for permission codes, instead of typing in a free text box. We hold group meeting discussed and make the decision of developing an search box for showing the courses need to apply for permission code. Moreover, during the client meeting, we asked the rule of this showing courses part, the client provide the rules: it will show the course list according to your applying time. If student apply this at the beginning of one year, it will show all course require a permission code in this year, if student apply this at the middle of one year, for example at the beginning of semester 2, it will only show second semester course require a permission code in this year. Because the course rules usually changed at the beginning of years. The output of this decision as below:
![Images](https://gitlab.cecs.anu.edu.au/u6125835/group_project_bindi/raw/master/Documents/Images/functional_improvement/04.png "4")
### Improvement 4:
To solve the problem of duplication request Email, we decided to add this feature to combine the different courses permission code request Emails. If you click Add course 2, it will show more information collection field for addition course. And according to the require from client, student could add no more than 4 course permission code request, as in most common situation, student could take 4 courses in each semester, it they need to overload, they should request via ISIS. The output of this feature as below:
![Images](https://gitlab.cecs.anu.edu.au/u6125835/group_project_bindi/raw/master/Documents/Images/functional_improvement/05.png "5")
### Improvement 5:
We also add a free text field for student to write some other information. According to feedback from student, they usually do not pay attention on reminder information written on the top of generated draft Email web page. After discussion with client, to effectively avoid duplication Email, we decided to add these two-tick box, if student do not tick these check box it will not generate the draft email. The output of this decision as below:
![Images](https://gitlab.cecs.anu.edu.au/u6125835/group_project_bindi/raw/master/Documents/Images/functional_improvement/06.png "6")
### Improvement 6:
The requirement of client is asking us to do not automatically send the Email via web page, because these kind of request Email should be sent via student Email. But during the testing phase, we also received some good suggestions to make the process more convenient for student. After group discussion, we added an ANU Email link on button ‘YES’. It allows student to copy the draft and then automatically jump to their ANU Email box to send this request Email. The reflection of this decision as below:
![Images](https://gitlab.cecs.anu.edu.au/u6125835/group_project_bindi/raw/master/Documents/Images/functional_improvement/07.png "7")

After click ‘YES’, it will jump to ANU Email box…

![Images](https://gitlab.cecs.anu.edu.au/u6125835/group_project_bindi/raw/master/Documents/Images/functional_improvement/08.png "8")
### Improvement 7:
During the client meeting, we found one more problem met by clients. They have a course database already, however, it is not user friendly for them to change. The program rules and course information are always changes. How to make this system easier to maintain and modify, after we graduate? So, we make a decision to develop a user-friendly admin port for staff in CECS to change this information. We developed demo of the admin parts with add, delete and update course information. The feature of this decision as below:

Show all courses:

![Images](https://gitlab.cecs.anu.edu.au/u6125835/group_project_bindi/raw/master/Documents/Images/functional_improvement/09.png "9")

Add:

![Images](https://gitlab.cecs.anu.edu.au/u6125835/group_project_bindi/raw/master/Documents/Images/functional_improvement/10.png "10")
![Images](https://gitlab.cecs.anu.edu.au/u6125835/group_project_bindi/raw/master/Documents/Images/functional_improvement/11.png "11")

Update:

![Images](https://gitlab.cecs.anu.edu.au/u6125835/group_project_bindi/raw/master/Documents/Images/functional_improvement/12.png "12")

Delete:

![Images](https://gitlab.cecs.anu.edu.au/u6125835/group_project_bindi/raw/master/Documents/Images/functional_improvement/13.png "13")
![Images](https://gitlab.cecs.anu.edu.au/u6125835/group_project_bindi/raw/master/Documents/Images/functional_improvement/14.png "14")

Future plan of permission code part:
1.	Enter all data into the database, approximate 10 hours. 
2.	Front-end web page data synchronize from database, approximate 5 hours.
