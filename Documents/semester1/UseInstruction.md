Version 1.0
------

Open the homepage, enter first name, last name and last name:

![Images](https://gitlab.cecs.anu.edu.au/u6125835/group_project_bindi/raw/master/Documents/Images/1.png "1")

Choose the degree and program:

![Images](https://gitlab.cecs.anu.edu.au/u6125835/group_project_bindi/raw/master/Documents/Images/2.png "2")

Enter the information of the course which is applied for the permission code:

![Images](https://gitlab.cecs.anu.edu.au/u6125835/group_project_bindi/raw/master/Documents/Images/3.png "3")

If permission codes for more than one course are needed, click the “Add/Delete Course” to add or remove courses:

![Images](https://gitlab.cecs.anu.edu.au/u6125835/group_project_bindi/raw/master/Documents/Images/4.png "4")

The maximum number of courses that could be applied within one application is 4. After all courses are added, click the “Confirm” button to create an email draft:

![Images](https://gitlab.cecs.anu.edu.au/u6125835/group_project_bindi/raw/master/Documents/Images/5.png "5")

Check the draft and make sure all the information is correct:

![Images](https://gitlab.cecs.anu.edu.au/u6125835/group_project_bindi/raw/master/Documents/Images/6.png "6")

Open your own ANU account email box page and create a new email sending to studentadmin@anu.edu.au with a title as “Request Permission Code(s) uxxxxxxx”:

![Images](https://gitlab.cecs.anu.edu.au/u6125835/group_project_bindi/raw/master/Documents/Images/7.png "7")

![Images](https://gitlab.cecs.anu.edu.au/u6125835/group_project_bindi/raw/master/Documents/Images/11.png "11")

Click the “Copy the Draft” button and paste as the content of the new email:

![Images](https://gitlab.cecs.anu.edu.au/u6125835/group_project_bindi/raw/master/Documents/Images/8.png "8")

![Images](https://gitlab.cecs.anu.edu.au/u6125835/group_project_bindi/raw/master/Documents/Images/12.png "12")

Then send the email.

Click the feedback button:

![Images](https://gitlab.cecs.anu.edu.au/u6125835/group_project_bindi/raw/master/Documents/Images/9.jpg "9")

Please give us a feedback about how you feeling this draft, which would help us to improve it later:

![Images](https://gitlab.cecs.anu.edu.au/u6125835/group_project_bindi/raw/master/Documents/Images/10.jpg "10")


##### Note:
Please DO NOT send duplicated Email for same application.
Please use your ANU Student Email Account to send that permission code application email.


Overall
------
feedback_page.html
could collect information.

system will generate a draft Email
show on email_draft.html

Then the page will jump to 
feedback_page.html
collect feedback from users (students)


