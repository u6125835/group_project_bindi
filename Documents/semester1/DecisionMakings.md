Decision Making
===========================

Week 3 
------
#### Client meeting:
* Collect lots of requires from clients, so we decide to organize these needs and write an requirement documentation.

#### Group meeting:
* After discussing and according to the some detailed requirements, we have a general idea of the entire system design.
* Study the feasibility of this design.
* Need to estimate the risk.

###### Outcomes later (according client and group meeting):
- [x] <a href="https://drive.google.com/open?id=1txtM0R60Vl5KeV1cwv9JEDGHLoUUEjhuXerZOBhUKdQ"> Requirements Analysis Document of Bindi </a>
- [x] <a href="https://drive.google.com/open?id=1L1eeqIZjOlI7WjAy_nQ4Xvm3yt0DP0JU"> Feasibility Study </a>
- [x] <a href="https://drive.google.com/open?id=1596pjznLrOi0Pa-1c0R8HOar3iDERaZ-"> Risk Assessment </a>

#### Assessment:
* Decide to look over the pre bindi project again.
* Learn the process of decision making

###### Outcomes later (according assessment):
- [x] <a href="https://drive.google.com/open?id=1Fs33jbwBULrg1ltJmXLy2pC2l0jaHsYfoKkijoMaODc"> Review of Main Outcome From the Former Bindi Team </a>
- [x] <a href="https://drive.google.com/open?id=1QINj9j1LirHHdQifJMlX04T_nJ4RVG6AF2rIXGdqofM"> Decision Making Process </a>

Week 4
------
#### Client meeting:
* Try to use agile project management to do bindi project.
* Learn what is agile? And How to use it?

#### Group meeting:
* Category the user story, and write the Backlog.
* Estimate the job time, and make a mile stone.

###### Outcomes later (according client and group meeting):
- [x] <a href="https://drive.google.com/open?id=1BTvygVZI--2AsDCUs-uQQiK5VFb_lAqsVb9myyWKqok"> Agile Estimate and Planning </a>
- [x] <a href="https://drive.google.com/open?id=1DrmpMCgZWp9L901VwnEPUnwBFKGr4KcJxdHpLQ_7AyU"> User story draft </a>
- [x] <a href="https://drive.google.com/open?id=10ba80vFosolFqm4yLSLEbdcY2v5SoTVX-5ulYRN0vTM"> Backlog & User story draft </a>
- [x] <a href="https://drive.google.com/open?id=1jrrvcL1mzEZN7UXcYqY3LWhJuzqxMwYI"> Mile stone </a>

Week 5 
------
#### Client meeting:
* Asking client more questions to find out more requirements (all requires contained in user story).
* Misunderstanding the backlog.
* Combine agile management in one place/tool (GitLab).

#### Group meeting:
* Update all user story and analysis the requires in each user story.
* Replan the job need to do.
* Reesimate the time for each stage, make a new milestone.

###### Outcomes later (according client and group meeting):
- [x] Create 24 user stories (Gitlab -> Issues -> List)
- [x] Create the Kanban board in gitlab (Gitlab -> Board)
- [x] Create the milestone in gitlab (Gitlab -> Milestone)

#### Tutorial:
* Make the UML diagram.
* Start codeing: Demo website, database design.

###### Outcomes later (according tutor suggestion):
- [x] ~~UML diagram of permission code part (Gitlab repo -> Documents)~~ 
- [x] Demo website of permission code and sent email (Gitlab repo -> Documents)
- [x] Database design and EER model graph (Gitlab repo -> database)

Week 6 Client meeting
------
#### Client meeting:
* Draw workflaw diagram.

#### Group meeting:
* Write use case documentation.
* Category the user stories, and do user story mapping.

###### Outcomes later (according client and group meeting):
- [x] use case documentation (Gitlab repo -> Documents)
- [x] user story mapping (Gitlab repo -> Documents)
- [x] workflaw diagram (Gitlab repo -> Documents)

#### Assessment:
* Move on coding part.
* Improve use case documentation.

###### Outcomes later (according assessment):
- [x] improve use case documentation as markdown file (Gitlab repo -> Documents -> UseCase.md)

Week 7
------
#### Group meeting:
* Set up the basic structure of project by using django.

#### Tutorial:
* Add reflection documentation record every reflection according to meeting and assessment.
* Add decision making documentation record every decision according to meeting and assessment.

###### Outcomes later (according tutor suggestion):
- [x] project setup - django
- [x] reflection documentation (Gitlab repo -> Documents -> reflection.md)
- [x] decision making documentation (Gitlab repo -> Documents -> DecisionMaking.md)

Week 8
------
#### Client meeting:
* Add a feedback collection page.
* Add some more details in information collection page.

#### Group meeting:
* Assign tasks to each other, clear the details of each page.
* Add a page generate email draft, and add a copy button for this draft, which will be convenient for user.

###### Outcomes later (according client and group meeting):
- [x] information collection page
- [x] email draft generate part
- [x] feedback collection page

Week 9 & 10
------
#### Client meeting:
* Improve the language and details in generate drafts.
* Delete complete course information, this will involved in later version (corressponding to CV recognize PDF or picture of academic history.
* Add Email title including uni id, e.g. "Request permission code/codes (uxxxxx)".
* Back button should not crash (it is ok now)
* Add specialization not only Master of Computing.. (add in later version v1.1)
* Error message, technique problems ..

#### Group meeting:
* Assign tasks to each other, clear the details of each page.
* Add a page generate email draft, and add a copy button for this draft, which will be convenient for user.

###### Outcomes later (according client and group meeting):
recent version 1.0
- [x] Add Email title in email_draft page
- [x] Improve the language in email_draft page
- [x] Delete complete course information show on pages
later version 1.1
- [ ] Add specialization
- [ ] Error message

 

