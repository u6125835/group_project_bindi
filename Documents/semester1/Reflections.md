Reflections
===========================

Week 3 Client meeting
------
1. We are forbidden to send the emails through other non-official email box. Only the ANU email is permitted to use;
2. No access to ANU database. It means if we want to arrange the course plan and reply the permission code as a draft for staff to send the email, we should choose the other way to get these information.
3. No database for storing the degree rules. We should get the data from Program&Cource by ourselves.
4. Plan the regular meeting time with Natalie.

##### reflection:
1. Set up the documentation in google driver using to record the meeting minutes and other essential documents;
2. Change the requirements in the Requirement Specification;
3. Commence to build our own database.

##### outcomes:
1. Finish the final version of Requirement Specification;
2. Move on design the Relations in the database.

Reflections of Week 3 Tutorial
------
* from our group:

>Our team recorded every feedback the client gave to us during the meetings and had used them to improve our requirement document. We also recorded the advice we got from the Audit Tutorials today and we will discuss it later.

>We only meet the client for 1 times now on this Tuesday. We don’t have opportunity to get some feedback from client now, but we are planning to get some feedback on next client meeting, and confirmed this design is suitable for client requirement.

>Some feedback we really listen to and try to improve it by next presentation.

>We paid much attention to the feedback from our client, tutors.

>we really pay much attention on the feedback from clients.

>We got a lot feedback and we’ll think about and learn from it.

* from tutor

>Make use of all feedback resources available to you. This includes the expertise of other teams. Consider running decisions and technical ideas by other teams during tutorials, as an example.


* from shadow group:

>Provide more information about individual contributions can offer a good overview of the works of the whole group

>In this tutorial they received a lot of feedback and their document has start to be changed on the suggested direction.

>Reflection could be shorter and clearly.

>First audit, they havent got feedback yet

>Get the pain point of student and try to solve them, listen to feedback from tutor and clients and even shadow teams and try to fix the problems.

>They have a clear documentation on what they are acting on. Also they have regular meeting that talks about feedback and comments from client.

##### reflection:
1. Insist on documentation updating and recording the meeting minutes;
2. Disorganise the relevant documentation;
3. More clear and short reflection;

##### outcomes:
1. Bagin to do the reflection job and learn the lessons from this audit to improve our project management in every sides.

Week 4 Client meeting
------
##### reflection:
1. Clients cannot understand what we write in our documentation including Requirement Document, Feasibility Analysis, Risk Analysis, System diagram
2. The deployment of all the documentation was disorganized. Therefore, it is not clear for clients to read them and connect them together.
3. Specific management method is needed in our group. It is really important for a project achieving success.

##### outcomes:
1. Discard what we have done before and learn to systematically manage our project;
2. Change our next week plan to learn the agile methodology according to client’s needs:
    * Mountaingoatsoftware.com
	* Scrum Methodology
	* Learn knowledge about KanBan
	* Complete user story
	* Complete backlog

Week 5 Client meeting
------
##### reflection:
1. Did not use the KanBan board in Gitlab and just put the output separately in different platforms;
2. Clearly understand the value for our clients
    * Every semester the student service can receive about 2000 emails
    * And there is rough 500 email related to asking permission code
    * After we complete this project, almost 20 hours can be saved 

##### outcomes:
1. 24 user stories
2. Backlog and milestone
3. Integrate all the documents and output into gitlab

Week 6 Client meeting
------
##### reflection:
1. Clear expected output by define the minimum value product:
    * Do not implement the login page for the students, just let students to input basic compulsory information to help to format the email draft
    * Do not send the email draft through our system. Students will copy and paste the email draft and send it through their own anu email account manually
2. More new requirements:
    * Make up the work flow for the project
    * Provide the VM for the web set-up

##### outcomes:
1. Change the implementation in the version 1;
2. Complete the work flow, user map and UML diagram;
3. Successfully implement the email service.

Reflection of Week 6 Tutorial
------
##### feedback: 
* from clients:
https://gitlab.cecs.anu.edu.au/u6125835/group_project_bindi/blob/master/Stakeholder%20Feedback/2018_S1_W6_Convenor_Evaluation_Bindi.pdf
* from tutor:

>Make use of all feedback resources available to you. This includes the expertise of other teams. Consider running decisions and technical ideas by other teams during tutorials, as an example.

* from our group:

>Our team recorded every feedback the client gave to us during the meetings and had used them to improve our requirement document. We also recorded the advice we got from the Audit Tutorials today and we will discuss it later.

>We only meet the client for 1 times now on this Tuesday. We don’t have opportunity to get some feedback from client now, but we are planning to get some feedback on next client meeting, and confirmed this design is suitable for client requirement.

>Some feedback we really listen to and try to improve it by next presentation.

>We paid much attention to the feedback from our client, tutors.

>We really pay much attention on the feedback from clients


* from shadow group:

>Provide more information about individual contributions can offer a good overview of the works of the whole group.

>In this tutorial they received a lot of feedback and their document has start to be changed on the suggested direction.

>Reflection could be shorter and clearly.

>First audit, they havent got feedback yet.

>Get the pain point of student and try to solve them, listen to feedback from tutor and clients and even shadow teams and try to fix the problems.

>They have a clear documentation on what they are acting on. Also they have regular meeting that talks about feedback and comments from client.


##### reflections: 
1. need to improve use case
2. user mapping is according to the user storys, give the connection with features of product
3. change the UML diagram
4. reassessment the risk in this project

##### outcomes:
1. Speed up our process since we are a little bit backward due to learing the agile management;
2. Learning how the implement the interaction between the front end and back end.

Week 7 Client meeting
------
no reflection, no meeting, because of illness of client

Week 8 Client meeting
------
##### reflections: 
1. show our workflow to the clients.

    >The overall design was satisfied by the clients. However, some details still need to be improve further to make the interacting more convenient for users.
2. UI demo of login page.

	>Add more radio boxes and check boxes about career choosing and subjects choosing.
	
3. After they have clearly looked at the user map, many feedback will be given at next time.

##### outcomes:
1. Get the VM from the CSIT administrator;
2. Improve the user map from the feedback of our clients.


Week 9 Client meeting
------
##### reflections: 
1. Database issure.

    >We focus on making all the system to run successfully. Therefore, the database would be present at the next stage.
2. VM machine.

	>Get the VM from the administrator to set up our server.
	
3. Arrange the next meeting.

    >Time, location and purpose of next meeting.

##### outcomes:
1. Make the system more stable and ensure it does not crush on the showcase.
2. Decide the specific room for the next Demo presentation.


Week 10 Client meeting
------
##### reflections: 
1. Information input part:
    * Student Name should be divided into First Name and Last Name;
    * Add the pre-course information of students and it includes the courses are not taken in the ANU;
    * Missing of major, minor and specialization for the specific program.

2. Draft part:
    * Language used in the draft email need to be improved;
    * Instead of using words like I had failed this course before, email should use “I have attempted this course twice”;
    * Lack of subject(title) in draft including the uni ID;
    * Better to add the back button to return the previous page;

3. Others
    * When occur some errors(technical problem), how the system can handle it

##### outcomes:
1. Last modification before the showcase and improve some features to meet the clients' requirements.
2. Improve the whole UI design and fix some bugs.
