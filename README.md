<div style="text-align:center">
   <a href="https://cecs.anu.edu.au/staff/student-services">
      <img alt="ANU Logo" src="https://gitlab.cecs.anu.edu.au/u6125835/group_project_bindi/raw/master/bindiproj/static/img/ANU%20image.png" width=600 height=200>
   <a>
</div>

Bindi
===========================
This project delivered a web-based AI assistant platform that automatically generate course permission code request and course scheduling plans for ANU students. The platform improves the efficiency of students’ course enrollment process.

Project specifics
------
##### Stakeholders:
Include, but are not limited to: 
* The client--CECS student service
* CECS students
* Members of Bindi group

##### Team member:

|Member|Email|Primary/Secondary Role|Deliverable
|-|---|---|---|
|**Peini Zhu**|u6125835@anu.edu.au|Back-end Developer/Leader|Back-end develop and database design
|**Yilun Liu**|u6111948@anu.edu.au|Spokeman/Back-end Developer|Holding meetings, communication with other association
|**Liuyang Qin**|u6141384@anu.edu.au|Documentation manager/Test manager| Meeting record, decision making, reflection update
|**Jiawei Liu**|u5893577@anu.edu.au|Front-end Developer/Hosting and Deployment|Web UI design
|**Wenjie Sun**|u5969555@anu.edu.au|Hosting and Deployment/Front-end Developer|Deployment and maintain host running
|**Wangyang Luo**|u5957547@anu.edu.au|Test manager/Documentation manager|Test script and sign-off report

##### Current Status:

* Passed test stage:

The test report can be found <a href="https://gitlab.cecs.anu.edu.au/u6125835/group_project_bindi/blob/master/Documents/Test%20Report2.md">**here**</a>.

The related feature requirements can be found <a href="https://gitlab.cecs.anu.edu.au/u6125835/group_project_bindi/blob/master/Documents/project_requirements.md">**here**</a>.

* Output:

Demo can be tried (Campus access only) <a href="http://150.203.186.138/">**here**</a>.

Give us more <a href="http://150.203.186.138/permission_code/feedback/">**Feedback**</a> to improve it! :)

* Risk assessment:
The risk assessment report can be found <a href="https://gitlab.cecs.anu.edu.au/u6125835/group_project_bindi/blob/master/Documents/Risk%20Analysis.md">**here**</a>.

##### Timeline:

|Week|Task|
|-|---|
|1 (Jul. 29)|Update based on clients’ feedback|
|2 (Aug. 5)|Database configuration|
|3 (Aug. 12)|Design function of course plan|
|4 (Aug. 19)|Django configuration for frontend; Modify css|
|5 (Aug. 26)|Server Configuration; Fix request method; Unit test|
|6 (Sep. 2)|Deployment; Add course search box and reminder window; Client test|
|Break (Sep. 16)|Database design; Basic back end development; UI update|
|7 (Sep. 23)|Administration page; Validate a course plan; Preparation for user test|
|8 (Sep. 30)|Generate a list of required course; Update index background; User tes|
|9 (Oct. 7)|Poster; Data for testing course planning; Add-course function update; Update based on users’ feedbcak|
|10 (Oct. 14)|Build a feasible course plan; Add link to Email box; Collect test final result|
|Future Plan|
|10 (Oct. 14)|Add data of courses to database; Webpage to show the course plan|
|11 (Oct. 21)|Webpage to edit program requirements and collect information and preference from students; Test|
|12 (Oct. 28)|Handover|

##### Target release: 

Week 12 of 2018 semester2 

##### Handover plan:

| Task                         | Due         | Expect Hrs. | Status     | Member |
| ---------------------------- |:-----------:|:-----------:|:----------:| ----------:|
| Enter all data into the database | Oct. 19     |    10        | Todo       |Peini Zhu   |
| Front-end web page data synchronize from database | Oct. 19     |    3        | Todo       |Peini Zhu   |
| Finish front-end for course arrange part | Oct. 19     |     10      | Doing       | Jiawei Liu  |
| Test for course arrange part | Oct. 19     |     5      | Todo       | Liuyang Qin  |
| Organize documents in repo | Oct. 26     |     5      | Todo       | Wangyang Luo  |
| Add docstring for code | Oct. 26     |    5        | Todo       |Peini Zhu & Yilun Liu  |
| Write code maintain instruction | Oct. 26     |    5        | Todo       |Peini Zhu & Yilun Liu  |
| Write admin use Instruction | Oct. 26     |    2        | Todo       |Peini Zhu   |
| Write course arrange use Instruction | Oct. 26     |    2        | Todo       |Yilun Liu   |
| Write host maintain Instruction | Oct. 26     |    2        | Todo       |Wenjie Sun   |

##### Notes: The permission code part is the primary function requested by client. While the course arrangement part is completely isolated from the main function, any contengency, delay or failure related to this part will not lead to the failure of the whole project.

Background and client objectives
------

##### Background 

Bindi is born from the Techlauncher, which is organized by ANU. It aims to assist the student service of College of Engineering and Computer Science(CECS).


##### Motivation and Risk background

On the beginning few weeks of one semester, the staff of CECS usually experienced a hard time. Because 

1. there are about thousands emails from students waiting to be replied;
2. 500 emails of them are related to asking permission code;
3. Unfortunately, Many of the emails are duplicated.

So many workload and no enough human resource result in some compliants from students. For example, the process of replying emails is slow, the reply message is inaccurate and unrelated and students did not get what they want.

In terms of students, a plenty of fresh students or even some old students do not have a clear plan of what courses they should enroll to fulfill the graduation criteria. In addition, the college of CECS does not provide a detailed requirements documentation. Therefore, on the beginning few weeks of one semester, students usually ask help for student service, which is no doubt that it will take a bit much time and effort for the staff with enough human resource.


Team goals and strategic
------

##### Scope of version one

The first phase of the Bindi system focus on assisting the staff to deal with these emails and reduce their workload at a peak time. The main idea about this is to formalize the email and make it complete and clear with the uniform format. This system has its own database to record the details of each students who use it. In addition, for different universities different course rules can be applied in Bindi which can ensure the portability and stability.

##### Feature of version one

First of all, Bindi collects the personal course information of students from their input and automatically generate the standard email that is clear and comprehensive for staff to read and confirm. Then Bindi will write a draft about the replying email that can be edited and used by our staffs. Moreover, Bindi can increase the efficiency significantly through filtering the same emails at the beginning. Therefore, no matter students or staffs, they all can receive the accurate messages, and send out an email with the uniform standard.

##### Scope of version two

Then, in this semester, course arrangement function will be added on Bindi as the second phase. It can help the student to arrange their course plan and get the ideas about what courses should be enrolled to fulfill the requirement of successful graduating. To achieve it, Bindi will provide a webpage for students to choose their courses based on their major or specialization. 

##### Feature of version two

Student fill the form to provide their information (program, first semester) and preference of courses (any courses in certain semesters or any semesters). Then the system will build several feasible course plans as output. Students can edit any plans provided by the system if they are not satisfied about them and re-submit it to system to get new plans based on their modification.


User interaction and design
------
* <a href="https://gitlab.cecs.anu.edu.au/u6125835/group_project_bindi/issues?label_name%5B%5D=User+Story"> **User Stories** </a>

* User Map:

<img alt="User map" src="https://gitlab.cecs.anu.edu.au/u6125835/group_project_bindi/raw/master/Documents/semester1/User%20Story%20Map.jpg">

* Workflow:

<img alt="Workflow" src="https://gitlab.cecs.anu.edu.au/u6125835/group_project_bindi/raw/master/Documents/Images/workflow.png">


* <a href="https://gitlab.cecs.anu.edu.au/u6125835/group_project_bindi/blob/master/Documents/project_requirements.md"> **Improved Requirement documentation** </a>

Reflections
------
As the team understands the problems to solve, they often have questions. Create a table of "things we need to decide or research" to track these items.
* <a href="https://gitlab.cecs.anu.edu.au/u6125835/group_project_bindi/blob/master/Documents/Reflections&Decisionmaking.md"> **Decision making** </a>

* <a href="https://gitlab.cecs.anu.edu.au/u6125835/group_project_bindi/blob/master/Documents/Functional_improvement.md"> **Improvement features of permission code request** </a>

* <a href="https://docs.google.com/document/d/10j2BxFwmGYZ8Jpb6YT4_IvzIXWzxnmA0h0b_KOqqPHU/edit"> **Host issue Log** </a>

Group plan 
------
Keep the team focused on the work at hand by clearly calling out what you're not doing. Flag things that are out of scope at the moment, but might be considered at a later time.
* **Group Plan**
    * <a href="https://gitlab.cecs.anu.edu.au/u6125835/group_project_bindi/blob/master/Documents/Group%20Plan/HostingPlan.md"> Hosting Plan </a>
    * <a href="https://gitlab.cecs.anu.edu.au/u6125835/group_project_bindi/blob/master/Documents/Group%20Plan/Front-endDevelopPlan.md"> Front-end Develop Plan </a>
    * <a href="https://gitlab.cecs.anu.edu.au/u6125835/group_project_bindi/blob/master/Documents/Group%20Plan/Back-endDevelopPlan.md"> Back-end Develop Plan </a>

Questions - What we're not doing?
------
* Issue 1:

We are not going to develop the function of automatically sending the Email via web page, according to the requirement of client, see more detail in <a href="https://gitlab.cecs.anu.edu.au/u6125835/group_project_bindi/blob/master/Documents/Functional_improvement.md"> **Improvement features of permission code request** </a>
No.7. We can add the link to make the process more convenient. 

* Issue 2:

From the test feedback, if student access our project website via third part application such as Wechat, it will cause "GET/POST Error", 
this is caused by the Wechat web browse design, which is out of our project scope. We will not going to fix this bug in recent stage, as we 
developed this project based on general browser (including mobile browser), this bug maybe will be fixed by later distribution of Wechat, 
or later improvement of our project.

* Issue 3:

As the different programs have different rules, according to the requirement of client, we only developed the course arrange for 
'Master of Computing'. The extend functions of other project are out of this project scope. This issue could be improved later, 
we will try to add enough docstring for code, and make an explanation documantion for code, which will be easier for others to improve it.

Achievement and value
------
* Filter duplicated permission code enquiring Emails. 
* Reduce process time of permission code enquiring Emails (from 5 to 2 mins per Email)
* Provide efficient method to generated permission code request
* Easy to expand the projects to other college (user-friendly admin module)

Approach
------
### Technical tools:

**Django** - development structs

**Gitlab** - repository of project

**Slack/ANU Email** - online communicate application

### Agile Management:

* [KANBAN Board](https://gitlab.cecs.anu.edu.au/u6125835/group_project_bindi/boards): prograess record with detailed lists and jobs
* [Milestones](https://gitlab.cecs.anu.edu.au/u6125835/group_project_bindi/milestones): Plan and progress record
* [Issues List](https://gitlab.cecs.anu.edu.au/u6125835/group_project_bindi/issues): Detailed jobs
* [Gitlab Repo](https://gitlab.cecs.anu.edu.au/u6125835/group_project_bindi/tree/master/bindiproj): Project codes
* [Gitlab Repo](https://gitlab.cecs.anu.edu.au/u6125835/group_project_bindi/tree/master/Documents): Project documentation

### Meeting records:

Semester 1

| Week |Client Meeting Notes |Group Meeting Notes|
|-|---|---|
|week1|[2018/02/22](https://gitlab.cecs.anu.edu.au/u6125835/group_project_bindi/blob/master/Documents/semester1/Meeting%20records/Client%20Meeting/Bindi%20Group%20Meeting%20Summary1.pdf)|[2018/02/22](https://gitlab.cecs.anu.edu.au/u6125835/group_project_bindi/blob/master/Documents/semester1/Meeting%20records/Team%20Meeting/Bindi%20Group%20Meeting%20Summary1.pdf)|
|week2|[2018/02/27](https://docs.google.com/document/d/1dU2mpSdT5jL2knJsNFlcjqn8JCyIL_cRqOpJYxVw6s8/edit)|[2018/02/27](https://gitlab.cecs.anu.edu.au/u6125835/group_project_bindi/blob/master/Documents/semester1/Meeting%20records/Team%20Meeting/Bindi%20Group%20Meeting%20Summary2.pdf)|
|week3|[2018/03/05](https://docs.google.com/document/d/1xmeiCcEKO7xAAHYYxObNzRu8NFc-WEs_j1GcuMwOHkg/edit)|[2018/03/07](https://gitlab.cecs.anu.edu.au/u6125835/group_project_bindi/blob/master/Documents/semester1/Meeting%20records/Team%20Meeting/Bindi%20Group%20Meeting%20Summary3.pdf)|
|week4|[2018/03/14](https://docs.google.com/document/d/1xmeiCcEKO7xAAHYYxObNzRu8NFc-WEs_j1GcuMwOHkg/edit)|[2018/03/14](https://gitlab.cecs.anu.edu.au/u6125835/group_project_bindi/blob/master/Documents/semester1/Meeting%20records/Team%20Meeting/Bindi%20Group%20Meeting%20Summary4.pdf)|
|week5|[2018/03/19](https://docs.google.com/document/d/1_IKYHASFUtulO6IOqsKFT4RfDDcg0l5kbREUmj2KI4o/edit)|[2018/03/21](https://gitlab.cecs.anu.edu.au/u6125835/group_project_bindi/blob/master/Documents/semester1/Meeting%20records/Team%20Meeting/Bindi%20Group%20Meeting%20Summary5.pdf)|
|week6|[2018/03/25](https://docs.google.com/document/d/1qsIg5m8eebm-DNJU_ssqUaUW0A_8TxISASJUhAPxnXQ/edit)|[2018/03/28](https://docs.google.com/document/d/1XtZ1GPW5ne9xsGI4-H3tgo7JPJ5BgQNcSBq0i0OiwWE/edit)|
|week7|[2018/04/16](https://docs.google.com/document/d/1pCf7CsB1rS4mzOwF2zrihKEb-nJeiUmgjOIks-czNTE/edit)|[2018/04/16](https://docs.google.com/document/d/1l4WFkgQFMiSZbGZzXOXHNYrVQu4tUdrqM5qxKvPVquw/edit)|
|week8||[2018/04/26](https://docs.google.com/document/d/1qsIg5m8eebm-DNJU_ssqUaUW0A_8TxISASJUhAPxnXQ/edit)|

Semester 2

| Week |Client Meeting Notes |Group Meeting Notes|
|-|---|---|
|week1|[2018/07/22](https://gitlab.cecs.anu.edu.au/u6125835/group_project_bindi/blob/master/Documents/Meeting%20records/Client%20Meeting/Bindi%20Client%20Meeting%20Summary%20week%201.md)|
|week2|[2018/08/03](https://gitlab.cecs.anu.edu.au/u6125835/group_project_bindi/blob/master/Documents/Meeting%20records/Client%20Meeting/Bindi%20Team%20Meeting%20Summary%20week%202.md)|[2018/08/03](https://gitlab.cecs.anu.edu.au/u6125835/group_project_bindi/blob/master/Documents/Meeting%20records/Team%20Meeting/Group%20Meeting%20week%202.md)|
|week3|[2018/08/10](https://gitlab.cecs.anu.edu.au/u6125835/group_project_bindi/blob/master/Documents/Meeting%20records/Client%20Meeting/Bindi%20Client%20Meeting%20Summary%20week%203.md) |[2018/08/10](https://gitlab.cecs.anu.edu.au/u6125835/group_project_bindi/blob/master/Documents/Meeting%20records/Team%20Meeting/Bindi%20Team%20Meeting%20Summary%20week%203.md)|
|week5|[2018/08/24](https://gitlab.cecs.anu.edu.au/u6125835/group_project_bindi/blob/master/Documents/Meeting%20records/Client%20Meeting/Bindi%20group%20client%20meeting%20week%205.md)|[2018/08/25](https://gitlab.cecs.anu.edu.au/u6125835/group_project_bindi/blob/master/Documents/Meeting%20records/Team%20Meeting/Bindi%20Group%20meeting%20week%204.md)|
|week6|[2018/09/01](https://gitlab.cecs.anu.edu.au/u6125835/group_project_bindi/blob/master/Documents/Meeting%20records/Client%20Meeting/Bindi%20Group%20Client%20Meeting%20Summary%20week6.md)|[2018/09/01](https://gitlab.cecs.anu.edu.au/u6125835/group_project_bindi/blob/master/Documents/Meeting%20records/Team%20Meeting/Bindi%20group%20meeting%20week%205.md)|
|week7|[2018/09/15](https://gitlab.cecs.anu.edu.au/u6125835/group_project_bindi/blob/master/Documents/Meeting%20records/Client%20Meeting/Bindi%20Group%20Client%20Meeting%20Week7.md)|[2018/09/15](https://docs.google.com/document/d/1S5sWr5sbpS4LhRZY5OxR2mzlV5ELqrJf_PJFbyDI7MA/edit)|
|week8|[2018/09/22](https://gitlab.cecs.anu.edu.au/u6125835/group_project_bindi/blob/master/Documents/Meeting%20records/Client%20Meeting/Bindi%20Client%20Meeting%20Summary%20Week8.md)|[2018/09/22](https://docs.google.com/document/d/1qki8MQ7qMf3W0EHJhqSX3otFmO63vZDt-BbU4d_hy5A/edit)|
|week9|[2018/10/02](https://gitlab.cecs.anu.edu.au/u6125835/group_project_bindi/blob/master/Documents/Meeting%20records/Client%20Meeting/Bindi%20Client%20Meeting%20Week9.md)|[2018/10/02](https://docs.google.com/document/d/1aS-FSWHJvh5n9sFSKfAU8oUKNUQibcHU6UdXFePOdmk/edit)|

