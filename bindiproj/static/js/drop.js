function allowDrop(ev)
{
    if(ev.target.id == "container" || ev.target.id == "1" || ev.target.id == "2" || ev.target.id == "3" || ev.target.id == "4"){
        return ;
    }
    ev.preventDefault();
}

function disallowDrag(id)
{
    var obj = document.getElementById("2");
    obj.draggable = false;
}
 
function drag(ev)
{
    ev.dataTransfer.setData("Text",ev.target.id);
}

function check() {
    // check rules, return undraggable course id.
    return ["2"];
}
 
function drop(ev)
{
    ev.preventDefault();
    var data=ev.dataTransfer.getData("Text");
    ev.target.appendChild(document.getElementById(data));

    // var obj = document.getElementById("2");
    // obj.draggable = false;

    var undraggable = check();

    for(var i = 0; i < undraggable.length; i++) {
        var obj = document.getElementById("2");
        obj.draggable = false;
    }
}