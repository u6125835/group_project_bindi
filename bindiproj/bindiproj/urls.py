"""bindiproj URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.conf.urls import url

from permissioncode import views as email_views
from coursearrange import views as course_views
from bindiadmin import views as admin_views

urlpatterns = [

    path('permission_code/', email_views.info, name='info_collect'),
    path('permission_code/email_draft/', email_views.email, name='email_draft'),
    path('permission_code/feedback/', email_views.feedback, name='feedback'),
    path('permission_code/feedback/insert/', email_views.insert_fb, name="insert_fb"),
    path('', email_views.index, name='index'),

    ############# admin manage rules demo functional parts --- need to add UI later #############

    path('bindiadmin/', admin_views.bindiadmin, name="admin_login"),
    path('bindiadmin/login/', admin_views.login, name="admin_login"),
    path('bindiadmin/logout/', admin_views.logout, name="admin_logout"),
    path('bindiadmin/index/', admin_views.index, name="admin_index"),

    path('testcourses/', admin_views.show_courses, name='showcourse'),
    path('testrules/', admin_views.show_rules, name='showrule'),
    path('testadd/', admin_views.add_rules, name='addrule'),
    path('testadd/insert/', admin_views.insert_rules, name='insert_rules'),

    path('testrules/delete/<int:cid>', admin_views.del_rules, name='del_rules'),

    path('testrules/<int:cid>', admin_views.edit_rules, name='edit_rules'),
    path('admin/', admin.site.urls),
]
