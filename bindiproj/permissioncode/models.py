from django.db import models

# Create your models here.

class Feedback(models.Model):
    score = models.IntegerField()
    comment = models.CharField(max_length=1000)

    def __str__(self):
        return self.score

