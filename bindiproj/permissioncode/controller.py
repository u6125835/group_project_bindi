import json

from django.shortcuts import render_to_response


def email_draft(name, student_id, program, target_course, completed_courses, additional):
    """
    Create the draft based on formatted data
    :param name: student full name
    :param student_id: student id
    :param program: degree of program, such as "Master of Computing"
    :param target_course:
    :param completed_courses: null 
    :param additional: additional information from info_form.html
    :return: dict of title and content of the email
    """

    basic_information = 'My name is {0} ({1}). I am currently completing the {2}.\n\n'.format(name, student_id, program)

    # the course student already completed, it is not implemented in current stage
    if completed_courses:
        cs = ''
        for c in completed_courses:
            cs += ', ' + c
        completed_course_information = 'I already have completed: ' + cs[2:] + '.\n'
    else:
        completed_course_information = ''

    # claim of whether attempted/failed the course before
    cs = ''
    failure_history = 'I confirm that '
    attempted = ''
    not_attempted = ''
    for k in target_course.keys():
        cs += ', ' + target_course[k]['course_id']
        if target_course[k]['failure'] == 'true':
            if target_course[k]['failure_times'] == 'true':
                attempted += target_course[k]['course_id']+'(with failure) '
            else:
                attempted += target_course[k]['course_id'] + '(without failure) '
        else:
            not_attempted += target_course[k]['course_id']+' '
    if attempted != '':
        failure_history += 'I have attempted the course(s): ' + attempted + 'previously'
    if not_attempted != '':
        tem = 'I have not attempted the course(s): ' + not_attempted
        if attempted != '':
            failure_history += '.\nAnd ' + tem
        else:
            failure_history += tem + '\n'

    target_course_information = 'I am requesting permission to enroll in the following course(s):\n{0}. \n\n{1}\n'.format(cs[2:], failure_history)

    end_infromation = '\nI understand that my request may take up to 7 days to process'

    body = basic_information + target_course_information + completed_course_information + end_infromation + '\n\n' + additional
    head = 'Hi CECS Student Services,\n\n'
    end = '\n \nRegards, \n'+'\n{0}'.format(name)
    draft = head+body+end
    title = "Permission code request - " + student_id
    draft_dic = {"content": draft, "title": title}
    # return render_to_response("email_draft.html", draft_dic)
    return draft_dic


def generate_email(request):
    """
    Get data from info_form.html
    :param request: 
    target_course: list of course requested for permission code
    fail_flag, f_f: if student have attended the related target course before
    fail_times, f_t: if student have failed the related target course before
    course: dict that generates related information of target course
    """
    first_name = request.POST.get('firstname')
    last_name = request.POST.get('lastname')
    student_id = request.POST.get('studentid')
    degree = request.POST.get('degree')
    program = request.POST.get('program')
    target_course = [request.POST.get('address1')]
    fail_flag = [request.POST.get('fail_flag1')]
    fail_times = [request.POST.get('fail_times1')]
    additional = request.POST.get('additional')
    degree_fixed = []
    if degree == '1':
        degree_fixed = 'Bachelor of '
    elif degree == '2':
        degree_fixed = 'Master of '
    # program_fixed = []
    if program == 'other':
        program = request.POST.get('programname')
    f_f = []
    f_t = []
    for i in range(3):
        j = 'c{0}'.format(i+2)
        if int(request.POST.get(j, 0)):  # add initial value 0
            # if the course2/3/4 are added for permission code request in the info_form.html
            address = 'address{0}'.format(i+2)
            fail_f = 'fail_flag{0}'.format(i+2)
            fail_t = 'fail_times{0}'.format(i+2)
            target_course.append(request.POST.get(address))
            fail_flag.append(request.POST.get(fail_f))
            fail_times.append(request.POST.get(fail_t))

    for i in range(len(target_course)):
        if fail_flag[i] == '1':
            f_f.append('true')
            if fail_times[i] == '1':
                f_t.append('true')
            else:
                f_t.append('false')
        elif fail_flag[i] == '2':
            f_f.append('false')
            f_t.append('false')
        # f_t.append(str(int(fail_times[i])-1))

    completed_courses = []

    name = first_name + " " + last_name
    # dop = degree_fixed + " of " + program_fixed
    courses = {}
    for i in range(len(target_course)):
        course = {}
        course['course_id'] = target_course[i]
        course['failure'] = f_f[i]
        course['failure_times'] = f_t[i]
        courseid = "course"+str(i+1)
        courses[courseid] = course
    draft_dic = email_draft(name, student_id, program, courses, completed_courses, additional)

    response = render_to_response("email_draft.html", draft_dic)
    response.set_cookie("uid", student_id)

    return response

