from django.shortcuts import render
from permissioncode.controller import generate_email
from permissioncode.models import Feedback
from django.shortcuts import render_to_response
from django.http import HttpResponse

from django.views.decorators.csrf import csrf_exempt


# Create your views here.
import permissioncode.models as p_models

@csrf_exempt
def index(request):
    return render(request, 'index.html')
    # return render(request, 'addrules.html')
def info(request):
    return render(request, 'info_form.html')

@csrf_exempt
def email(request):
    return generate_email(request)

@csrf_exempt
def feedback(request):
    return render(request, 'feedback_page.html')


@csrf_exempt
def insert_fb(request):
    score = request.POST.getlist('score')[0]
    comment = request.POST['comment']
    Feedback.objects.create(score=score, comment=comment)
    return render_to_response('feedback_page.html', {'score': score, 'comment': comment})
