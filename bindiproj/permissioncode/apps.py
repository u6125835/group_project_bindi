from django.apps import AppConfig


class DraftemailConfig(AppConfig):
    name = 'draftemail'
