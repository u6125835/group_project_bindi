from django.shortcuts import render
from django.shortcuts import render_to_response

from django.http import HttpResponse, HttpResponseRedirect


# Create your views here.

def jumpTo_courseArrangement(request):
    """
    relocate to courseArrangement page
    :return:
    """
    return render(request, "course_arrangement.html")
