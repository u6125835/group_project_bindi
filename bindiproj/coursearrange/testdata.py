# reuqirement:   program: name of the program
#               time: the version of the requirement (17:2017, 18:2018, etc)
#               specialization: name of specialization
#               units: required units for the program
#               compulsory: list of compulsory courses
#               level: required units for different level of course (like 6000 and 8000)
#               title: required units for certain title of course; 'other': course with different title but still count
#               group_and: list of several groups, all the groups must be satisfied
#               group_or: list if several groups, at least one group should be satisfied
#               for a group, the integer means the units of the courses that group requires
#                   following the integer is the optional courses in that group
# over6units: courses that count over 6 units (should be enrolled for multiple times)
#           'str':[a,b,c]: str is the code of the course; a is the required units;
#               b is the minimal units for one semester; c is the maximal units for one semester
# simultaneity: couses that can be enrolled in the same semester with its pre-course
# course_lib:   keys are the course codes
#               'name': name of the course
#               'unit': unit of the course
#               'title': title of the course (such as COMP, MATH)
#               'level': level of the course
#               'offer_time': offer time of the course (1801: 2018 semester 1)
#               'precondition': pre condition of the course
#                   'course':[[]] at least one course from each list should be satisfied
#                   'program': required program
#                   'incompatibility': list of incompatible courses

requirement=[{'program':'Master of Computing', 'time':17, 'specialization':'AI','units':96,
              'compulsory':['COMP6250','COMP8260','COMP6442'],'level':{'8000':0},'title':[{'COMP':36},{'other':[]}],
              'group_and':[[24,'COMP6320','COMP6365','COMP6490','COMP8420','COMP8600','COMP8620','COMP8650','COMP8670']],
              'group_or':[[[12,'COMP8715','COMP8715'],[12,'COMP8755','COMP8755']]]},
             {'program': 'Master of Computing', 'time': 18, 'specialization': 'None', 'units': 96,
              'compulsory': ['COMP6250', 'COMP8260', 'COMP6442', 'COMP6710', 'COMP8110', 'MATH6005'],
              'level': {'8000': 0}, 'title': [{'COMP': 30},{'other':['ENGN6528']}],
              'group_and': [[6, 'COMP6120', 'COMP8190'], [6, 'COMP6240', 'COMP6420'], [6, 'COMP6331', 'COMP6340']],
              'group_or': [[[12,'COMP8715','COMP8715'],[12,'COMP8755','COMP8755']]]},
             {'program': 'Master of Computing', 'time': 18, 'specialization': 'AI', 'units': 96,
              'compulsory': ['COMP6250', 'COMP8260', 'COMP6442', 'COMP6710', 'COMP8110', 'MATH6005'],
              'level': {'8000': 0}, 'title': [{'COMP': 6},{'other':[]}],
              'group_and': [[6, 'COMP6120', 'COMP8190'], [6, 'COMP6240', 'COMP6420'], [6, 'COMP6331', 'COMP6340'],
                            [12, 'COMP6260', 'COMP6262', 'COMP6320'],[12, 'COMP8420', 'COMP8600', 'COMP8620', 'COMP8650', 'COMP8670', 'ENGN6528']],
              'group_or': [[[12,'COMP8715','COMP8715'],[12,'COMP8755','COMP8755']]]},
             {'program': 'Master of Computing', 'time': 18, 'specialization': 'DS', 'units': 96,
              'compulsory': ['COMP6250', 'COMP8260', 'COMP6442', 'COMP6710', 'COMP8110', 'MATH6005','COMP8410','COMP8430','COMP6490'],
              'level': {'8000': 0}, 'title': [{'COMP': 6},{'other':[]}],
              'group_and': [[6, 'COMP6120', 'COMP8190'], [6, 'COMP6240', 'COMP6420'], [6, 'COMP6331', 'COMP6340'],
                            [6, 'COMP6320', 'COMP8600', 'COMP8620', 'COMP8650', 'COMP8420']],
              'group_or': [[[12,'COMP8715','COMP8715'],[12,'COMP8755','COMP8755']]]}]
over6units={'COMP8715': [12,6,6], 'COMP8755': [12,6,12], 'COMP8800': [24,12,12]}
simultaneity={'COMP8715':['COMP6442']}
course_lib = {'COMP':{'name': 'COMP', 'unit': 6, 'title': 'COMP', 'level': 0, 'offer_time': 'all', 'pre_condition':{'course': [[]], 'program':[], 'incompatibility':[]}},
'ENGN6528':{'name': 'DMM', 'unit': 6, 'title': 'MATH', 'level': 6000, 'offer_time': [1801, 1802, 1901, 1902], 'pre_condition':{'course': [[]], 'program':[], 'incompatibility':[]}},
'MATH6005':{'name': 'DMM', 'unit': 6, 'title': 'MATH', 'level': 6000, 'offer_time': [1801, 1802, 1901, 1902], 'pre_condition':{'course': [[]], 'program':[], 'incompatibility':[]}},
'COMP6120':{'name': '6120', 'unit': 6, 'title': 'COMP', 'level': 6000, 'offer_time': [1801,1802, 1901,1902, 2002, 2102], 'pre_condition':{'course': [[]], 'program':[], 'incompatibility':[]}},
'COMP6240':{'name': 'Rational Database', 'unit': 6, 'title': 'COMP', 'level': 6000, 'offer_time': [1802, 1902, 2002, 2102], 'pre_condition':{'course': [[]], 'program':[], 'incompatibility':[]}},
'COMP6250':{'name': 'PP1', 'unit': 6, 'title': 'COMP', 'level': 6000, 'offer_time': [1701,1702, 1801,1802, 1901,1902, 2002, 2001,2102, 2101], 'pre_condition':{'course': [[]], 'program':[], 'incompatibility':[]}},
'COMP6260':{'name': 'Foundation of computing', 'unit': 6, 'title': 'COMP', 'level': 6000, 'offer_time': [1802, 1902, 2002, 2102], 'pre_condition':{'course': [[]],'program':[], 'incompatibility':[]}},
'COMP6261':{'name': 'Information Theory', 'unit': 6, 'title': 'COMP', 'level': 6000, 'offer_time': [1802, 1902, 2002, 2102], 'pre_condition':{'course': [[]], 'program':[], 'incompatibility':[]}},
'COMP6262':{'name': 'Logic', 'unit': 6, 'title': 'COMP', 'level': 6000, 'offer_time': [1801, 1901, 2001, 2101], 'pre_condition':{'course': [[]],'program':[], 'incompatibility':[]}},
'COMP6300':{'name': 'Computer Organisation', 'unit': 6, 'title': 'COMP', 'level': 6000, 'offer_time': [1801, 1901, 2001, 2101], 'pre_condition':{'course': [[]],'program':[], 'incompatibility':[]}},
'COMP6310':{'name': 'Systems and Networks', 'unit': 6, 'title': 'COMP', 'level': 6000, 'offer_time': [1802, 1902, 2002, 2102], 'pre_condition':{'course': [[]],'program':[], 'incompatibility':[]}},
'COMP6320':{'name': 'AI', 'unit': 6, 'title': 'COMP', 'level': 6000, 'offer_time': [1701, 1801, 1901, 2001, 2101], 'pre_condition':{'course': [[]],'program':[], 'incompatibility':[]}},
'COMP6330':{'name': 'Operating Systems', 'unit': 6, 'title': 'COMP', 'level': 6000, 'offer_time': [1802,  2002, 2102], 'pre_condition':{'course': [[]], 'program':[], 'incompatibility':[]}},
'COMP6331':{'name': 'Computer Networks', 'unit': 6, 'title': 'COMP', 'level': 6000, 'offer_time': [1801,  2001, 2101], 'pre_condition':{'course': [[]],'program':[], 'incompatibility':[]}},
'COMP6340':{'name': 'Network Information System', 'unit': 6, 'title': 'COMP', 'level': 6000, 'offer_time': [1801, 1901, 2001, 2101], 'pre_condition':{'course': [[]], 'program':[], 'incompatibility':[]}},
'COMP6353':{'name': 'Systems Engineering', 'unit': 6, 'title': 'COMP', 'level': 6000, 'offer_time': [1802], 'pre_condition':{'course': [], 'program':[], 'incompatibility':[]}},
'COMP6361':{'name': 'Principles of Programming', 'unit': 6, 'title': 'COMP', 'level': 6000, 'offer_time': [1702, 1902, 2102], 'pre_condition':{'course': [[]],'program':[], 'incompatibility':[]}},
'COMP6365':{'name': 'Principles of Programming', 'unit': 6, 'title': 'COMP', 'level': 6000, 'offer_time': [1701, 1901, 2101], 'pre_condition':{'course': [[]],'program':[], 'incompatibility':[]}},
'COMP6390':{'name': 'Human Computer Interface', 'unit': 6, 'title': 'COMP', 'level': 6000, 'offer_time': [1802, 1902, 2002, 2102, 2202], 'pre_condition':{'course': [[]],'program':[], 'incompatibility':[]}},
'COMP6420':{'name': 'ISML', 'unit': 6, 'title': 'COMP', 'level': 8000, 'offer_time': [1801,1901],'pre_condition':{'course': [['COMP6710']], 'program':[], 'incompatibility':[]}},
'COMP6442':{'name': 'SOFTWARE DESIGN METHOD', 'unit': 6, 'title': 'COMP', 'level': 6000, 'offer_time': [1801,1802, 1901,1902, 2002, 2001,2102, 2101], 'pre_condition':{'course': [['COMP6700','COMP6710']],'program':[], 'incompatibility':[]}},
'COMP6461':{'name': 'Computer Graphics', 'unit': 6, 'title': 'COMP', 'level': 6000, 'offer_time': [1702, 1902], 'pre_condition':{'course': [[]],'program':[], 'incompatibility':[]}},
'COMP6464':{'name': 'High performance', 'unit': 6, 'title': 'COMP', 'level': 6000, 'offer_time': [1801, 2001,2201], 'pre_condition':{'course': [[]],'program':[], 'incompatibility':[]}},
'COMP6466':{'name': 'Algorithms', 'unit': 6, 'title': 'COMP', 'level': 6000, 'offer_time': [1802, 1902, 2002, 2102], 'pre_condition':{'course': [[]],'program':[], 'incompatibility':[]}},
'COMP6490':{'name': 'DA', 'unit': 6, 'title': 'COMP', 'level': 6000, 'offer_time': [1702, 1802, 1902, 2002, 2102], 'pre_condition':{'course': [[]],'program':[], 'incompatibility':[]}},
'COMP6700':{'name': 'Structed programming', 'unit': 6, 'title': 'COMP', 'level': 6000, 'offer_time': [1701,1801], 'pre_condition':{'course': [[]],'program':[], 'incompatibility':[]}},
'COMP6710':{'name': 'Structed programming', 'unit': 6, 'title': 'COMP', 'level': 6000, 'offer_time': [1801,1802, 1901,1902, 2002, 2001,2102, 2101], 'pre_condition':{'course': [[]],'program':[], 'incompatibility':[]}},
'COMP6719':{'name': 'Computing for engineering simulation', 'unit': 6, 'title': 'COMP', 'level': 6000, 'offer_time': [1801, 2001,2201], 'pre_condition':{'course': [[]],'program':[], 'incompatibility':[]}},
'COMP6720':{'name': 'Art and interaction', 'unit': 6, 'title': 'COMP', 'level': 6000, 'offer_time': [1802, 1902], 'pre_condition':{'course': [[]],'program':[], 'incompatibility':[]}},
'COMP6730':{'name': 'Programming for Scientist', 'unit': 6, 'title': 'COMP', 'level': 6000, 'offer_time': [1802, 1902,1801,1901], 'pre_condition':{'course': [[]],'program':[], 'incompatibility':[]}},
'COMP6780':{'name': 'Web Development', 'unit': 6, 'title': 'COMP', 'level': 6000, 'offer_time': [1801, 11901], 'pre_condition':{'course': [[]],'program':[], 'incompatibility':[]}},
'COMP8110':{'name': 'MSP', 'unit': 6, 'title': 'COMP', 'level': 8000, 'offer_time': [1801, 1901, 2001],'pre_condition':{'course': [[]], 'program':['Master of Computing'], 'incompatibility':[]}},
'COMP8180':{'name': 'SSS', 'unit': 6, 'title': 'COMP', 'level': 8000, 'offer_time': [1803, 2003, 2203],'pre_condition':{'course': [[]],'program':['Master of Computing', 'Master of Engineering'], 'incompatibility':[]}},
'COMP8190':{'name': 'MSD', 'unit': 6, 'title': 'COMP', 'level': 8000, 'offer_time': [1802, 2002, 2202],'pre_condition':{'course': [[]], 'program':['Master of Computing'], 'incompatibility':[]}},
'COMP8260':{'name': 'PP2', 'unit': 6, 'title': 'COMP', 'level': 8000, 'offer_time': [1702, 1801, 1802, 1901, 1902, 2001, 2002, 2101, 2102],'pre_condition':{'course': [['COMP6250']], 'program':['Master of Computing'], 'incompatibility':[]}},
'COMP8410':{'name': 'DM', 'unit': 6, 'title': 'COMP', 'level': 8000, 'offer_time': [1801, 1901, 2001],'pre_condition':{'course': [['COMP6240'],['COMP6710','COMP6730']],'program':['Master of Computing'], 'incompatibility':['COMP8400']}},
'COMP8420':{'name': 'DM', 'unit': 6, 'title': 'COMP', 'level': 8000, 'offer_time': [1801, 1901, 2001],'pre_condition':{'course': [[]], 'exception':['Master of Computing','Master of ADA'], 'program':[], 'incompatibility':[]}},
'COMP8430':{'name': 'DW', 'unit': 6, 'title': 'COMP', 'level': 8000, 'offer_time': [1802,1902],'pre_condition':{'course': [['COMP6730', 'COMP6710'],['COMP6240','COMP6420']], 'program':[], 'incompatibility':[]}},
'COMP8460':{'name': 'AA', 'unit': 6, 'title': 'COMP', 'level': 8000, 'offer_time': [1802],'pre_condition':{'course': [[]],'program':[], 'incompatibility':[]}},
'COMP8600':{'name': 'ISML', 'unit': 6, 'title': 'COMP', 'level': 8000, 'offer_time': [1801,1901,2001,2101],'pre_condition':{'course': [[]], 'program':[], 'incompatibility':[]}},
'COMP8620':{'name': 'ISML', 'unit': 6, 'title': 'COMP', 'level': 8000, 'offer_time': [2101],'pre_condition':{'course': [['COMP6320']], 'program':[], 'incompatibility':[]}},
'COMP8650':{'name': 'ISML', 'unit': 6, 'title': 'COMP', 'level': 8000, 'offer_time': [1802,1901],'pre_condition':{'course': [['COMP8600']], 'program':[], 'incompatibility':[]}},
'COMP8670':{'name': 'AL', 'unit': 6, 'title': 'COMP', 'level': 8000, 'offer_time': [1702],'pre_condition':{'course': [[]], 'program':[], 'incompatibility':[]}},
'COMP8715':{'name': 'CP', 'unit': 6, 'title': 'COMP', 'level': 8000, 'offer_time': [1801,1802,1901,1902,2001,2002,2101,2102],'pre_condition':{'course': [['COMP8260'],['COMP6442']],'program':['Master of Computing'], 'incompatibility':['COMP8830']}},
'COMP8755':{'name': 'ICP', 'unit': 6, 'title': 'COMP', 'level': 8000, 'offer_time': [1801,1802,1901,1902],'pre_condition':{'course': [['COMP8260'],['COMP6442']],'program':['Master of Computing'], 'incompatibility':[]}},
'COMP8800':{'name': 'CRP', 'unit': 6, 'title': 'COMP', 'level': 8000, 'offer_time': [1801,1802,1901,1902,2001,2002,2101,2102],'pre_condition':{'course': [['COMP6442'],['COMP8260']],'program':[], 'incompatibility':[]}}}

import courseplanner as cp
# input:
#       spec: specialization
#       prog: program
#       start: version of the requirement
#       time: first year of the student
#       seme: first semester of the student
#       exception: list of courses that can ignore its pre condition
#       course_plan: initial plan
req={}
spec = 'AI'
prog = 'Master of Computing'
start = 18
time = 18
seme = 1
exception = []
for i in requirement:
    if i['program'] == prog and i['time'] == start and i['specialization'] == spec:
        req = i
        break

s1 = time*100+seme
if seme == 1:
    s2 = s1+1
    s3 = s2+99
    s4 = s3+1
else:
    s2 = s1+99
    s3 = s2+1
    s4 = s3+99
course_plan = {0:[], s1: [], s2: [], s3: [], s4: []}

# clss=cp.request_list(course_plan,req)[0]
# cls=cp.resort(clss,course_lib, exception, prog)
# cl=cp.resort(cls,course_lib, exception, prog)


r = cp.plan(course_plan, req, course_lib, exception, over6units, simultaneity, prog)
for f in r:
    print(f)









# clss=['COMP6120', 'COMP6240', 'COMP6331', 'COMP6260', 'COMP6262', 'COMP8420', 'COMP8650', 'COMP8755', 'COMP8755', 'COMP6250', 'COMP8260', 'COMP6442', 'COMP6710', 'COMP8110', 'MATH6005']
# cls=cp.resort(clss,course_lib, exception, prog)
# cl=cp.resort(cls,course_lib, exception, prog)
# semester_m = cp.course_matrix(cl, course_lib, course_plan)
# res=cp.arrange(semester_m, cl, course_lib, exception, simultaneity, prog, over6units)
# print(res)

# r = cp.request_list(course_plan,req)
# for c in r:
#     print(c)
#     semester_m = cp.course_matrix(c, course_lib, course_plan)
#     res = cp.arrange(semester_m, c, course_lib, exception, simultaneity, prog, over6units)
#     print(res)
# starter=[0]*10
# clss=['COMP6700', 'COMP6320', 'COMP6365', 'COMP6490', 'COMP8420', 'COMP8715', 'COMP8715', 'COMP6250', 'COMP8260', 'COMP6442']
# cls=cp.resort(clss,course_lib, exception, prog)
# cl=cp.resort(cls,course_lib, exception, prog)
# print(cls)
# print(cl)
# option_m=[[[1, 0, 0, 0]], [[0, 1, 0, 0]], [[1, 0, 0, 0]], [[0, 1, 0, 0]], [[0, 0, 1, 0]], [[0, 0, 0, 1]],
#           [[0, 0, 1, 0]], [[1, 0, 0, 0]], [[0, 1, 0, 0]], [[0, 1, 0, 0]]]
# f = cp.feasible(cl, course_lib, exception, simultaneity, option_m, starter, prog, 9)
# print(f)

# import pulp as pulp
#
# def solve_ilp(objective, constraints):
#     pro = pulp.LpProblem('LP1', pulp.LpMaximize)
#     pro += objective
#     for con in constraints:
#         pro += con
#     res = pro.solve()
#     if res != 1:
#         return None
#     else:
#         return  pro.variables()

# n = 64
# varname = []
# for i in range(n):
#     for j in range(4):
#         s = '0{}'.format(i*4+j)
#         varname.append(s[-2:])
# variables = [pulp.LpVariable(varname[i], lowBound=0, cat=pulp.LpInteger) for i in range(64)]
# c =[1]*64
# objective = sum([c[i]*variables[i] for i in range(n)])
# constraints=[]
# a=[[1,0,0,0],
#    [1,0,1,0],
#    [0,0,1,0],
#    [1,0,1,0],
#    [1,1,1,1],
#    [1,1,1,1],
#    [1,0,0,0],
#    [1,1,1,1],
#    [0,1,0,1],
#    [1,0,1,0],
#    [0,0,0,1],
#    [0,0,1,1],
#    [1,0,1,0],
#    [1,0,1,0],
#    [0,1,0,1],
#    [0,1,1,0]]
#
# for i in range(16):
#     for j in range(4):
#         constraints.append(variables[i*4+j] <= a[i][j])
# for i in range(16):
#     constraints.append(sum(variables[i*4+k] for k in range(4))==1)
# for i in range(4):
#     constraints.append(sum(variables[j*4+i] for j in range(16))<=4)
# r = solve_ilp(objective,constraints)
# for i in range(len(r)):
#     v=r[i]
#     print(v.name,'=',v.varValue)