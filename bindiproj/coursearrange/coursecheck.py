"""
    from database:
    course={course code:, unit:{6}, level:{1000,2000,3000,4000,5000,6000,7000,8000}, offer_time:{},
    pre_condition:{course:, program:, incompatibility:}}
    requirement={units, compulsory list, level, group_and list, group_or list}
    
    from html:
    request={semester:{course code}, program, specialization, minor, major}
"""

# def course_match(request, course):
#     """
#     read course from html and match with course information from database
#     :return
#      course_matched[course_code]:
#         [0]: (int/str) unit
#         [1]: (int/str) level
#         [2]: (list) offer time
#         [3]: (set/list) pre course
#                 true: pre; false: parallel
#         [4]: (str) pre program
#         [5]: (set/list) incompatibility
#         [6]: (list) semester
#     """
#     # i: semester; j: course in semester i
#     course_matched = {}
#     for i in range():
#         for j in range():
#             string = ''
#             name = string+'{0}-{1}'.format(i,j)
#             course = request.POST.get(name)
#             course_matched[course].append()
#
#     return course_matched
#
#
# def require(request):
#     """
#         read program from html and match with information from database
#         :return
#          requirement:
#             units, compulsory list, level, group_and list, group_or list
#         """
#     program = request.POST.get('')
#     specialization = request.POST.get('')
#     major = request.POST.get('')
#     minor = request.POST.get('')
#     requirement={}
#     requirement['units'] =
#     requirement['compulsory'].append()
#     requirement['1000'] =
#     requirement['8000'] =
#     requirement['group_a'].append()
#     requirement['group_o'].append()
#
#     return requirement


def units(course_matched, requirement):
    """ :return unit_req: [0]: flag [1]: chosen [2]: required"""
    unit_req=[]
    chosen = 0
    flag = 0
    for c in course_matched.keys():
        chosen += course_matched[c][0]
    if chosen >= requirement['units']:
        flag = 1
    unit_req.append(flag)
    unit_req.append(chosen)
    unit_req.append(requirement['units'])
    return unit_req


def compulsory(course_matched, requirement):
    """:return compulsory_req: [0]:flag [1]: missing courses"""
    compulsory_req=[]
    missing=[]
    flag = 0
    for c in requirement['compulsory']:
        if c not in course_matched.keys():
            missing.append(c)
    if not missing:
        flag = 1
    compulsory_req.append(flag)
    compulsory_req.append(missing)
    return compulsory_req


def level(course_matched, requirement):
    """:return level_req['1000']: [0]:flag [1]: chosen [2]: required"""
    level_req={}
    for i in range(8):
        le = '{0}000'.format(i+1)
        flag = 0
        chosen = 0
        required = requirement[le]
        for c in course_matched.keys():
            if course_matched[c][1] == le:
                chosen += 1
        if chosen >= required:
            flag = 1
        level_req[le].append(flag)
        level_req[le].append(chosen)
        level_req[le].append(required)
    return level_req


def group(course_matched, course_list, number):
    chosen = 0
    flag = 0
    for c in course_matched.keys():
        if c in course_list:
            chosen += 6
    if chosen >= number:
        flag = 1
    return flag


def group_or(course_matched, requirement):
    flag = 1
    for go in requirement['group_or']:
        flag2 = 0
        cl = []
        for l in go:
            number = l[0]
            cl = l[1:]
            flag2 += group(course_matched, cl, number)
        flag = flag * flag2
    return flag


def group_and(course_matched, requirement):
    flag = 1
    for ga in requirement['group_and']:
        number = ga[0]
        cl = ga[1:]
        flag = falg * group(course_matched, cl, number)
    return flag


def pre_condition(request, course_matched):
    flags = 1
    program = request.POST.get('')
    for c in course_matched.keys():
        flag = 0
        if course_matched[c][3]:
            if course_matched[c][4] != program:
                for pc in course_matched[c][3]:
                    if pc[0] not in course_matched.keys():
                        flag = 0
                        return flag
                    else:
                        cs = min(course_matched[c][6])
                        maxpcs = max(course_matched[pc[0]][6])
                        minpcs = min(course_matched[pc[0]][6])
                        if pc[1]:
                            if cs <= maxpcs:
                                flag = 0
                                return flag
                            else:
                                flag = 1
                        else:
                            if cs < minpcs:
                                flag = 0
                                return flag
                            else:
                                flag = 1
            else:
                flag = 1
        else:
            flag = 1
        flags = flag*flags
    return flags


def incompatibility(course_matched):
    flag = 1
    for c in course_matched.keys():
        if course_matched[c][5]:
            for ic in course_matched[c][5]:
                if ic in course_matched.keys():
                    flag = 0
                    return flag
    return flag


def offer_time(course_matched):
    flag = 1
    for c in course_matched.keys():
        if course_matched[c][6] not in course_matched[c][2]:
            flag = 0
            return flag
    return flag


def duplicated(course_matched, exception):
    flag = 1
    for c in course_matched.keys():
        if c not in exception:
            if len(course_matched[c][6])>1:
                flag = 0
                return flag
    return flag
