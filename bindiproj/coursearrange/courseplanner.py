import itertools
import copy
import pulp
# import sys
#
# sys.setrecursionlimit(1000000)


def plan(course_plan, req, course_lib, exception, over6units, simul, program):
    """
    Data structure and examples can be found in testdata.py
    Generate several feasible course plans
    :param course_plan:  currently uncompleted course plan
    :param req: requirement of the program
    :param course_lib: courses data
    :param exception: list of course that ignore the pre condition
    :param over6units: course that can be enrolled for multiple time
    :param simul: course that can be enrolled with its pre course at the same semester
    :param program: program
    :return: several feasible course plans
    """
    course_final = []
    sem = []
    final_plan = []
    for k in course_plan.keys():
        if k:
            sem.append(k)

    course_picked = request_list(course_plan, req)
    for course_list in course_picked:
        add = add_precourse(course_list, course_lib, exception, program)
        for a in add:
            added = add_precourse(a, course_lib, exception, program)
            for cl in added:
                course_final.append(redundant(cl, over6units))

    for course_l in course_final:
        if len(course_l) > 16:
            re = "overload"
        else:
            course_li = resort(course_l, course_lib, exception, program)
            course_list = resort(course_li, course_lib, exception, program)
            semester_m = course_matrix(course_list, course_lib, course_plan)
            # semester_m2 = project(semester_m, over6units)
            res = arrange(semester_m, course_list, course_lib, exception, simul, program, over6units)
            if res[0][0] >= 0:
                solution = {}
                for s in sem:
                    solution[s] = []
                for i in range(len(course_list)):
                    for j in range(4):
                        if res[i][j]:
                            solution[sem[j]].append(course_list[i])
                for s in sem:
                    m = 4 - len(solution[s])
                    for i in range(m):
                        solution[s].append('COMP')
                final_plan.append(copy.deepcopy(solution))
            if len(final_plan) > 5:
                return final_plan
    if len(final_plan) == 0:
        print('no feasible plan')
    return final_plan


def resort(course_l, course_lib, exception, program):
    # sort the course list to have pre courses before other course in the list
    count1={}
    count2={}
    for course in course_l:
        count1[course] = 0
        count2[course] = 0
    for course in course_l:
        count1[course] += 1
    course_list = []
    for course in course_l:
        if course not in exception and course_lib[course]['pre_condition']['course'][0] and \
                ('exception' not in course_lib[course]['pre_condition'].keys()
                 or program not in course_lib[course]['pre_condition']['exception']):
            for pre_list in course_lib[course]['pre_condition']['course']:
                for c in pre_list:
                    if c in course_l and count2[c] < count1[c]:
                        course_list.append(c)
                        count2[c] += 1
            if count2[course] < count1[course]:
                course_list.append(course)
                count2[course] += 1
    for c in course_l:
        if count2[c] < count1[c]:
            course_list.append(c)
    return course_list


def course_matrix(course_list, course_lib, course_plan):
    """
    Create a matrix to reflect the relationship between course and semester
    """
    semester_m = []
    s = []
    n = len(course_list)
    for k in course_plan.keys():
        if k:
            s.append(k)
    for i in range(n):
        flag = 1
        course = course_list[i]
        t = [0, 0, 0, 0]
        for j in range(4):
            # if the course is request by student in a specific semester i, t[i-1]=1
            if course in course_plan[s[j]]:
                t[j] = 1
                flag = 0
        if flag:
            # For any course and its related vector t,
            # if the course is offered in the i-th semester, t[i-1]=1, otherwise t[i-1]=0
            for j in range(4):
                if s[j] in course_lib[course]['offer_time']:
                    t[j] = 1
        semester_m.append(copy.deepcopy(t))
    return semester_m


def arrange(semester_m, course_list, course_lib, exception, simul, program, over6units):
    n = len(semester_m)
    r = build_ilp(semester_m, course_list, course_lib, exception, simul, program, over6units)
    # option_m = []
    # res = []
    # for i in range(n):
    #     option_m.append([])
    #     res.append([])
    # starter = [0] * n
    # for k in range(n):
    #     for j in range(4):
    #         if semester_m[k][j]:
    #             b = [0, 0, 0, 0]
    #             b[j] += 1
    #             option_m[k].append(copy.deepcopy(b))
    # r = select_a_combination(option_m, starter, [0, 0, 0, 0], 0, course_list, course_lib, exception, simul, program, over6units)
    if r[0][0] == -1:
        # not solution found
        return [[-1]]
    else:
        return r


# def project(semester_m, over6units):
#     # over6_count = 0
#     # if course_list[inde] in over6units.keys():
#     #     over6_count += 1
#     #     maxi = int(over6units[course_list[inde]][0] / over6units[course_list[inde]][1])
#     #     mani = int(over6units[course_list[inde]][0] / over6units[course_list[inde]][2])
#     #     if maxi == 1:
#     #         if mini == 2:
#     return semester_m
#
#
# def select_a_combination(option_m, starter, semester, inde, course_list, course_lib, exception, simul, program, over6units):
#     n = len(option_m)
#     if inde == -1:
#         return [[-1]]
#     elif inde == n:
#         res = []
#         for i in range(n):
#             res.append(copy.deepcopy(option_m[i][starter[i]]))
#         return res
#
#     c = [0, 0, 0, 0]
#     alert = 0
#     m = starter[inde]
#     if m == len(option_m[inde]):
#         starter[inde] = 0
#         inde -= 1
#         for i in range(4):
#             semester[i] -= option_m[inde][starter[inde]][i]
#         starter[inde] += 1
#         return select_a_combination(option_m, starter, semester, inde, course_list, course_lib, exception, simul, program, over6units)
#     else:
#         for i in range(4):
#             c[i] = semester[i] + option_m[inde][m][i]
#             if c[i] > 4:
#                 alert = 1
#         if alert:
#             starter[inde] += 1
#             return select_a_combination(option_m, starter, semester, inde, course_list, course_lib, exception, simul, program, over6units)
#         elif feasible(course_list, course_lib, exception, simul, option_m, starter, program, inde):
#             for i in range(4):
#                 semester[i] = c[i]
#             inde += 1
#             return select_a_combination(option_m, starter, semester, inde, course_list, course_lib, exception, simul, program, over6units)
#         else:
#             inde = inde - 1
#             for i in range(4):
#                 semester[i] -= option_m[inde][starter[inde]][i]
#             starter[inde] += 1
#             return select_a_combination(option_m, starter, semester, inde, course_list, course_lib, exception, simul, program, over6units)
#
#
# def feasible(course_list, course_lib, exception, simul, option_m, starter, program, inde):
#     n = inde-1
#     flag = []
#     final = 1
#     for i in range(n):
#         flag.append(1)
#     for i in range(n):
#         course = course_list[i]
#         # if course not in exception and course_lib[course]['pre_condition']['course'][0]:
#         if course not in exception and course_lib[course]['pre_condition']['course'][0] and \
#                 ('exception' not in course_lib[course]['pre_condition'].keys()
#                  or program not in course_lib[course]['pre_condition']['exception']):
#             course_time = option_m[i][starter[i]]
#             f1 = 1
#             for pre_list in course_lib[course]['pre_condition']['course']:
#                 f2 = 0
#                 for pre_course in pre_list:
#                     for j in range(n):
#                         if pre_course == course_list[j]:
#                             pre_time = option_m[j][starter[j]]
#                             ct = course_time[0]*10000 + course_time[1]*100 + course_time[2]*10 + course_time[3]
#                             pt = pre_time[0]*10000 + pre_time[1]*100 + pre_time[2]*10 + pre_time[3]
#                             if pt > ct:
#                                 f2 = 1
#                             elif course in simul.keys():
#                                 if pre_course in simul[course]:
#                                     if pt == ct:
#                                         f2 = 1
#                 f1 = f1 * f2
#             flag[i] = f1
#     for f in flag:
#         final = final * f
#     return final


def redundant(course_list, over6units):
    """
    Remove redundant course from the list
    :param course_list: target list of courses
    :param over6units:  course that can be enrolled for multiple time
    """
    count = {}
    for course in course_list:
        count[course] = 0
    for course in course_list:
        count[course] += 1
    for k in count.keys():
        limit = 1
        if k in over6units.keys():
            limit = int(over6units[k][0]/6)
        for i in range(count[k]-limit):
            course_list.remove(k)
    return course_list


def add_precourse(course_list, course_lib, exception, program):
    """
    Add pre-courses to the course list
    """
    group_list = []
    for course in list(set(course_list).difference(set(exception))):
        # if course_lib[course]['pre_condition']['course'][0]:
        if course_lib[course]['pre_condition']['course'][0] and \
                ('exception' not in course_lib[course]['pre_condition'].keys()
                 or program not in course_lib[course]['pre_condition']['exception']):
            pre_course = course_lib[course]['pre_condition']['course']
            flag = 1
            flag2 = []
            for pre_list in pre_course:
                flag2.append(0)
                for c in course_list:
                    if c in pre_list:
                        flag2[-1] = 1
            for f in flag2:
                flag = flag * f
            if not flag:
                for i in range(len(flag2)):
                    if not flag2[i]:
                        group = []
                        for e in pre_course[i]:
                            group.append(copy.deepcopy([e]))
                        group_list.append(copy.deepcopy(group))
    if group_list:
        course_lists = pickone(group_list, course_list)
        return course_lists
    else:
        return [course_list]


def request_list(course_plan, req):
    """
    Generate a list of courses that should be in the plan from the requirement of program (req)
    and request from student (course_ plan)
    """
    # compulsory courses list
    comp_list = []
    for v in course_plan.values():
        for c in v:
            comp_list.append(c)
    for c in req['compulsory']:
        comp_list.append(c)

    # group_list: a certain number of courses must be chosen from the group
    group_list = []
    for ga in req['group_and']:
        gal = list(itertools.combinations(ga[1:], int(ga[0]/6)))
        group_list.append(copy.deepcopy(gal))
    for go in req['group_or']:
        gls = []
        for g in go:
            gl = list(itertools.combinations(g[1:], int(g[0]/6)))
            for gli in gl:
                gls.append(copy.deepcopy(gli))
        group_list.append(copy.deepcopy(gls))
    course_picked = pickone(group_list, comp_list)
    return course_picked


def pickone(group_list, comp_list):
    """
    From several groups of courses, choose one course from each group, return all the
    possible combination.
    """
    n = []
    course_list = []
    for g in group_list:
        n.append(len(g))
    m = len(n)
    c = 1
    for i in n:
        c = c * i
    r = {}
    for i in range(c):
        r[i] = []

    while m:
        c1 = 1
        for i in range(m-1):
            c1 = c1*n[i]
        c2 = int(c/c1/n[m-1])
        count = 0
        for i in range(c1):
            for j in range(n[m-1]):
                for k in range(c2):
                    r[count].append(j)
                    count += 1
        m = m-1

    for v in r.values():
        v.reverse()
        course = []
        group_number = 0
        for ind in v:
            for c in group_list[group_number][ind]:
                course.append(c)
            group_number += 1
        course = course + comp_list
        course_list.append(course)
    return course_list


def solve_ilp(objective, constraints):
    # solve the integer linear problem
    pro = pulp.LpProblem('ilp', pulp.LpMaximize)
    pro += objective
    for con in constraints:
        pro += con
    res = pro.solve()
    if res != 1:
        return -1
    else:
        return pro.variables()


def build_ilp(semester_m, course_list, course_lib, exception, simul, program, over6units):
    """
    Build the model of Integer Linear Problem
    :param semester_m: course_matrix
    :param course_lib: course data
    :param exception: course that its pre condition is ignored
    Constrains:
    course not in over6units: only enrolled in one semester
    course only enrolled in offered semester or the specific semester already request by student
    no overload: no more than 4 courses in one semester
    """
    n = len(semester_m)
    varname = []
    c = [1] * 4 * n
    constraints = []
    for i in range(n):
        for j in range(4):
            s = '0{}'.format(i*4+j)
            varname.append(s[-2:])
    variables = [pulp.LpVariable(varname[i], lowBound=0, cat=pulp.LpInteger) for i in range(4*n)]
    # objective = sum([c[i] * variables[i] for i in range(n * 4)])
    objective = 0
    for i in range(n):
        for j in range(4):
            constraints.append(variables[i * 4 + j] <= semester_m[i][j])
    for i in range(n):
        constraints.append(sum(variables[i * 4 + k] for k in range(4)) == 1)
    for i in range(4):
        constraints.append(sum(variables[j * 4 + i] for j in range(n)) <= 4)

    # for couser in over6units list (for example, COMP8715), there are two types:
    # 1. course that can/should be enrolled in the same semester twice
    # 2. course that should be enrolled in different semesters
    for i in range(n):
        course = course_list[i]
        # if course not in exception and course_lib[course]['pre_condition']['course'][0]:
        if course not in exception and course_lib[course]['pre_condition']['course'][0] and \
                ('exception' not in course_lib[course]['pre_condition'].keys()
                 or program not in course_lib[course]['pre_condition']['exception']):
            for pre_list in course_lib[course]['pre_condition']['course']:
                pre_time = 11111
                inde = -1
                for pre_course in pre_list:
                    for j in range(n):
                        if pre_course == course_list[j]:
                            p_t = semester_m[j][0]*10000+semester_m[j][1]*1000+semester_m[j][2]*100+semester_m[j][3]*10
                            if p_t < pre_time:
                                pre_time = p_t
                                inde = j
                if inde > -1:
                    pre = course_list[inde]
                    if course in simul and pre in simul[course]:
                        constraints.append(variables[inde * 4] * 10000 + variables[inde * 4 + 1] * 1000 + variables[
                                               inde * 4 + 2] * 100 + variables[inde * 4 + 3] * 10 >=
                                           variables[i * 4] * 10000 + variables[i * 4 + 1] * 1000 + variables[
                                               i * 4 + 2] * 100 + variables[i * 4 + 3] * 10)
                    else:
                        constraints.append(variables[inde * 4] * 10000 + variables[inde * 4 + 1] * 1000 + variables[
                            inde * 4 + 2] * 100 + variables[inde * 4 + 3] * 10 >=
                                           variables[i * 4] * 10000 + variables[i * 4 + 1] * 1000 + variables[
                                               i * 4 + 2] * 100 + variables[i * 4 + 3] * 10 + 1)
        if course in over6units.keys():
            maxi = int(over6units[course][2] / 6)
            mini = int(over6units[course][1] / 6)
            record = []
            for j in range(n):
                if course_list[j] == course:
                    record.append(j)
            if maxi == 1:
                for j in range(4):
                    constraints.append(sum(variables[k*4+j] for k in record) <= 1)
                for j in range(len(record)-1):
                    constraints.append(variables[record[j] * 4] >= variables[record[j + 1] * 4])
                    for k in range(3):
                        constraints.append(variables[record[j]*4+k] == variables[record[j+1]*4+k+1])
            if mini == 2:
                for j in range(int(len(record)/2)):
                    for k in range(4):
                        constraints.append(variables[record[j]*4+k] == variables[record[j+int(len(record)/2)]*4+k])

    solution = solve_ilp(objective, constraints)
    if solution == -1:
        return [[-1]]
    else:
        res = []
        for i in range(n):
            res.append([solution[i*4].varValue, solution[i*4+1].varValue, solution[i*4+2].varValue, solution[i*4+3].varValue])
        return res

