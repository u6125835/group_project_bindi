from django.shortcuts import render,render_to_response
from django.http import HttpResponse,HttpResponseRedirect
from django.template import RequestContext
from bindiadmin.forms import AdminForm
from bindiadmin.models import Admin, Course

from django.views.decorators.csrf import requires_csrf_token
from django.views.decorators.csrf import csrf_exempt

# Create your views here.

def bindiadmin(request):
    return render(request, 'admin_login.html')

def login(request):
    if request.method == 'POST':
        adminForm = AdminForm(request.POST)
        if adminForm.is_valid():
            uniid = adminForm.cleaned_data['id']
            password = adminForm.cleaned_data['pw']
            # compare to database admins
            adminuser = Admin.objects.filter(username__exact = uniid,password__exact = password)
            if adminuser:
                response = HttpResponseRedirect('/bindiadmin/index/')
                # write uniid to cookie, invaild in 3600s
                response.set_cookie('username', uniid, 3600)
                return response
            else:
                return HttpResponseRedirect('/bindiadmin/login/')
    else:
        adminForm = AdminForm()
    return render_to_response('login.html',{'adminForm':adminForm},context_instance=RequestContext(req))

def logout(request):
    response = HttpResponse('logout !!')
    # clean cookies
    response.delete_cookie('username')
    return response

def index(request):
    username = request.COOKIES.get('username','')
    return render_to_response('addrules.html' ,{'username':username})


def show_courses(request):
    courses = Course.objects.all()
    compulsorys = Course.objects.filter(category="Compulsory")
    selective_softwares = Course.objects.filter(category="Selective-Software")
    selective_databases = Course.objects.filter(category="Selective-Database")
    selective_networks = Course.objects.filter(category="Selective-Network")
    selective_projects = Course.objects.filter(category="Selective-Project")

    # return render(request,'table.html',{'form':table_form})
    return render_to_response("showcourses.html", locals())

def show_rules(request):
    courses = Course.objects.all()
    return render_to_response("showadminrules.html", locals())

def add_rules(request):
    return render(request, 'addrules.html')

@csrf_exempt
def insert_rules(request):
    code = request.POST['code']
    name = request.POST['name']
    category = request.POST['category']
    offering_time = request.POST['offering_time']
    previous_courses = request.POST['previous_courses']
    Course.objects.create(code= code, name = name, category= category, offering_time= offering_time, previous_courses= previous_courses)
    return render_to_response('addrules.html', {'code': code, 'name': name, 'category':category, 'offering_time': offering_time, 'previous_courses':previous_courses})

def edit_rules(request, cid):
    course = Course.objects.filter(id=cid)

    return render(request, 'addrules.html')

def del_rules(request, cid):
    Course.objects.filter(id=cid).delete()
    return HttpResponseRedirect('/testrules/')