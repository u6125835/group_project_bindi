from django import forms

class AdminForm(forms.Form):
    username = forms.CharField(label='Admin uni ID',max_length=100)
    password = forms.CharField(label='Admin password',widget=forms.PasswordInput())