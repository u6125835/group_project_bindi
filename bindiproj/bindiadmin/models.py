from django.db import models

# Create your models here.

class Admin(models.Model):
    uniid = models.CharField(max_length=30)
    password = models.CharField(max_length=30)

    def __str__(self):
        return self.uniid


class Course(models.Model):
    code = models.CharField(max_length=30)
    name = models.CharField(max_length=100)
    category = models.CharField(max_length=100)  # Compulsory / Specializations (AI, DS, HCI)/ Elective
    offering_time = models.CharField(max_length=50)  # semester 1/2/ spring/ summer/ autumn/ winter session/
    previous_courses = models.CharField(max_length=500)  # previous courses for this course, if no set "NULL", otherwise "C_Code1,C_Code2..."
    # other situation COMP6442 could be studied with COMP8715 at same semester

    ######### Notes ###########
    # Design a way for client to add rules
    # All the category of rules
    # database structure of CECS record course informations.

    def __str__(self):
        return self.code

class Program(models.Model):
    name = models.CharField(max_length=300)
    degree = models.CharField(max_length=30)

    def __str__(self):
        return self.name