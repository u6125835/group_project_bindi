How to set for MySQL?
------
### settings.py

    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.mysql',
            'NAME': 'bindidb', # your database name, pleas create in first time
            'USER': 'root', # your mysql user
            'PASSWORD': 'your password', # your mysql password
            'HOST': '127.0.0.1',
            'PORT': '3306',
        }
    }

How to migrate models?
------
    python manage.py makemigrations
    python manage.py migrate

How to import data?
------
    python manage.py shell

    from permissioncode.models import Program
    
    Program.objects.create(name="Diploma of Computing", degree="Undergraduate")
    Program.objects.create(name="Bachelor of Engineering (Honours)", degree="Undergraduate")
    Program.objects.create(name="Bachelor of Engineering (Honours) (Research and Development)", degree="Undergraduate")
    Program.objects.create(name="Bachelor of Advanced Computing (Honours)", degree="Undergraduate")
    Program.objects.create(name="Bachelor of Advanced Computing  (Honours) (Research and Development)", degree="Undergraduate")
    Program.objects.create(name="Bachelor of Applied Data Analytics", degree="Undergraduate")
    Program.objects.create(name="Bachelor of Software Engineering", degree="Undergraduate")
    Program.objects.create(name="Bachelor of Information Technology", degree="Undergraduate")
    Program.objects.create(name="Graduate Diploma of Computing", degree="Postgraduate")
    Program.objects.create(name="Master of Computing", degree="Postgraduate")
    Program.objects.create(name="Master of Computing (Advanced)", degree="Postgraduate")
    Program.objects.create(name="Graduate Diploma of Applied Data Analytics", degree="Postgraduate")
    Program.objects.create(name="Master of Applied Data Analytics", degree="Postgraduate")
    Program.objects.create(name="Master of Engineering in Digital Systems and Telecommunications", degree="Postgraduate")
    Program.objects.create(name="Master of Engineering in Mechatronics", degree="Postgraduate")
    Program.objects.create(name="Master of Engineering in Photonics", degree="Postgraduate")
    Program.objects.create(name="Master of Engineering in Renewable Energy Systems", degree="Postgraduate")
    Program.objects.create(name="Master of Innovation and Professional Proctice", degree="Postgraduate")
    Program.objects.create(name="Graduate Diploma of Cyber Security, Strategy and Risk Management", degree="Postgraduate")
    Program.objects.create(name="Master of Cyber Security, Strategy and Risk Management", degree="Postgraduate")

    from permissioncode.models import Course

    Course.objects.create(code="COMP6250", name="Professional Practice 1", category="Compulsory", offering_time="Both Semester", previous_courses="")
    Course.objects.create(code="COMP6442", name="Software Construction", category="Compulsory", offering_time="Both Semester", previous_courses="")
    Course.objects.create(code="COMP6710", name="Structured Programming", category="Compulsory", offering_time="Both Semester", previous_courses="")
    Course.objects.create(code="COMP8110", name="Managing Software Projects in a System Context", category="Compulsory", offering_time="First Semester", previous_courses="")
    Course.objects.create(code="COMP8260", name="Professional Practice 2", category="Compulsory", offering_time="Both Semester", previous_courses="")
    Course.objects.create(code="MATH6005", name="Discrete Mathematical Models", category="Compulsory", offering_time="First Semester", previous_courses="")

    Course.objects.create(code="COMP6120", name="Software Engineering", category="Selective-Software", offering_time="Second Semester", previous_courses="NULL")
    Course.objects.create(code="COMP8190", name="Model-Driven Software Development", category="Selective-Software", offering_time="Second Semester", previous_courses="NULL")
    Course.objects.create(code="COMP6240", name="Relational Databases", category="Selective-Database", offering_time="Second Semester", previous_courses="NULL")
    Course.objects.create(code="COMP6420", name="Introduction to Data Management, Analysis and Security", category="Selective-Database", offering_time="First Semester", previous_courses="NULL")
    Course.objects.create(code="COMP6331", name="Computer Networks", category="Selective-Network", offering_time="First Semester", previous_courses="NULL")
    Course.objects.create(code="COMP6340", name="Networked Information Systems", category="Selective-Network", offering_time="First Semester", previous_courses="NULL")
    Course.objects.create(code="COMP8715", name="Computing Project", category="Selective-Project", offering_time="Both Semester", previous_courses="COMP6250,COMP8260")
    Course.objects.create(code="COMP8755", name="Individual Computing Project", category="Selective-Project", offering_time="Both Semester", previous_courses="NULL")